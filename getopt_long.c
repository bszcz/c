// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

enum {
	alphaID = 'a',
	betaID = 'b'
};

int main(int argc, char** argv) {
	for (int i = 0; i < argc; i++) {
		printf("argv[%d] = '%s' \n", i, argv[i]);
	}

	int alpha = -1;
	double beta = -0.001;

	const struct option longopts[] = {
		{"alpha", required_argument, 0, alphaID},
		{"beta",  required_argument, 0, betaID},
		{0, 0, 0, 0}
	};

	for (;;) {
		int longindex = -1;
		int id = getopt_long(argc, argv, "a:b:", longopts, &longindex);

		if (-1 == id) {
			break;
		}

		printf("--\n");
		printf("id = %c, optarg = %s, longindex = %d", id, optarg, longindex);
		if (-1 != longindex) {
			printf(", longopts[%d].name = %s", longindex, longopts[longindex].name);
		}
		printf("\n");

		switch (id) {
		case alphaID:
			printf("alpha = %d \n", (int)strtol(optarg, NULL, 10));
			break;
		case betaID:
			printf("beta = %f \n", strtod(optarg, NULL));
			break;
		case '?':
			printf("???\n");
			break;
		default:
			printf("!!!\n");
		}
	}

	return 0;
}
