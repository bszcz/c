asm(" # Copyright (c) 2011 Bartosz Szczesny <bszcz@bszcz.org>");
asm(" # This program is free software under the MIT license.");

// compile with:
//
// gcc -S -O0 asm_multiply_shift.c -o - | grep -v ^# > asm_multiply_shift.s
//
// (grep bit makes assembler code look more readable)
// (why -O0 ??? try to compile with -O1 optimisation)

int main(void) {
	asm(" # a = 1 {");
	int a = 1;
	asm(" # } a = 1");

	asm(" # a = a*2 {");
	a = a*2;
	asm(" # } a = a*2");

	asm(" # a = a*3 {");
	a = a*3;
	asm(" # } a = a*3");

	asm(" # a = a*4 {");
	a = a*4;
	asm(" # } a = a*4");

	asm(" # a = a*5 {");
	a = a*5;
	asm(" # } a = a*5");

	asm(" # a = a*6 {");
	a = a*6;
	asm(" # } a = a*6");

	asm(" # a = a*7 {");
	a = a*7;
	asm(" # } a = a*7");

	asm(" # a = a*8 {");
	a = a*8;
	asm(" # } a = a*8");

	asm(" # return 0 {");
	return 0;
	asm(" # } return 0");
}
