// Copyright (c) 2011, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int inum1 = 0;
	int inum2 = 0;
	int iadd = 0;
	int isub = 0;
	int imul = 0;
	int iquo = 0;
	int irem = 0;

	printf("\nEnter two integers: ");
	scanf("%d%d", &inum1, &inum2);

	asm("addl  %%ebx, %%eax;" : "=a"(iadd) : "a"(inum1), "b"(inum2));
	asm("subl  %%ebx, %%eax;" : "=a"(isub) : "a"(inum1), "b"(inum2));
	asm("imull %%ebx, %%eax;" : "=a"(imul) : "a"(inum1), "b"(inum2));
	asm(
		"movl $0, %%edx;"
		"movl %2, %%eax;"
		"movl %3, %%ebx;"
		"idivl    %%ebx;"
		: "=a"(iquo), "=d"(irem)
		: "g"(inum1), "g"(inum2)
	);

	printf("%d + %d = %d\n",  inum1, inum2, iadd);
	printf("%d - %d = %d\n",  inum1, inum2, isub);
	printf("%d * %d = %d\n",  inum1, inum2, imul);
	printf("%d / %d = %d\n",  inum1, inum2, iquo);
	printf("%d %% %d = %d\n", inum1, inum2, irem);

	double dnum1 = 0.0;
	double dnum2 = 0.0;
	double dadd = 0.0;
	double dsub = 0.0;
	double dmul = 0.0;
	double ddiv = 0.0;

	printf("\nEnter two real numbers: ");
	scanf("%lf%lf", &dnum1, &dnum2);

	asm("faddp;" : "=t"(dadd) : "0"(dnum1), "u"(dnum2));
	asm("fsubp;" : "=t"(dsub) : "0"(dnum1), "u"(dnum2));
	asm("fmulp;" : "=t"(dmul) : "0"(dnum1), "u"(dnum2));
	asm("fdivp;" : "=t"(ddiv) : "0"(dnum1), "u"(dnum2));

	printf("%f + %f = %f\n", dnum1, dnum2, dadd);
	printf("%f - %f = %f\n", dnum1, dnum2, dsub);
	printf("%f * %f = %f\n", dnum1, dnum2, dmul);
	printf("%f / %f = %f\n", dnum1, dnum2, ddiv);

	exit(EXIT_SUCCESS);
}
