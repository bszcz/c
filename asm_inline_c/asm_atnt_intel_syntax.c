// Copyright (c) 2011, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

/*
---------------------------------------------------------
ATnT                          | Intel
---------------------------------------------------------
int $0x80                     | int 80h
mov $1, %eax                  | mov eax, 1
mov $0xff, %eax               | mov eax, 0ffh
mov %ebx, %eax                | mov eax, ebx
mov (%ecx), %eax              | mov eax, [ecx]
mov 3(%ebx), %eax             | mov eax, [ebx+3]
mov 0x20(%ebx), %eax          | mov eax, [ebx+20h]
mov (%ebx,%ecx), %eax         | mov eax, [ebx+ecx]
mov (%ebx,%ecx,0x2), %eax     | mov eax, [ebx+ecx*2h]
sub -0x20(%ebx,%ecx,0x4),%eax | sub eax, [ebx+ecx*4h-20h]
---------------------------------------------------------
*/

// ATnT : compile without anything
// Intel: compile with -masm=intel

#define USE_INTEL_SYNTAX 0

#include <stdio.h>
#include <stdlib.h>

// EAX register is the return value
#if USE_INTEL_SYNTAX
int Intel_syntax(void) {
	asm("mov eax, 13");
}
#else
int ATnT_syntax(void) {
	asm("mov $17, %eax");
}
#endif

int main(void) {
#if USE_INTEL_SYNTAX
	printf("Intel: returned value is %d.\n", Intel_syntax());
#else
	printf("AT&T : returned value is %d.\n", ATnT_syntax());
#endif
	exit(EXIT_SUCCESS);
}
