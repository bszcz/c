// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

/*
Constraints:
"r"     store in registers
"m"     store in memory
"=r"    '=' means output
"a"     store in %%eax
"b"     store in %%ebx
"c"     store in %%ecx
"d"     store in %%edx
"S"     store in %%esi
"D"     store in %%edi

asm volatile (...) - will not be optimised
__asm__, __volatile__ - POSIX compliant forms
*/

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	// A variable can be both input and output.
	// Output and input operands are numbered.
	// Use reference numbers for input constraints.
	// First operand "=a" has reference number "0".
	int i = 3;
	int array[2] = {11, 13};
	asm("inc %0;" : "=a"(i) : "0"(i));
	asm(
		"movl %1, %%eax;"
		"shl  %%eax;"
		"movl %%eax, %0;"
		: "=r"(array[1])
		: "r"(array[0])
		: "%eax" // Clobbered registers - will change.
	);
	printf("i = %d, array[0] = %d, array[1] = %d\n", i, array[0], array[1]);

	// Dereferncing in asm() is done with '()'.
	asm("movl (%1), %0;" : "=r"(i) : "r"(array));
	printf("i = array[0] = %d\n", i);

	exit(EXIT_SUCCESS);
}
