	.file	"asm_multiply_shift.c"
	 # Copyright (c) 2011 Bartosz Szczesny <bszcz@bszcz.org>
	 # This program is free software under the MIT license.
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	 # a = 1 {
	movl	$1, -4(%rbp)
	 # } a = 1
	 # a = a*2 {
	sall	-4(%rbp)
	 # } a = a*2
	 # a = a*3 {
	movl	-4(%rbp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	%eax, -4(%rbp)
	 # } a = a*3
	 # a = a*4 {
	sall	$2, -4(%rbp)
	 # } a = a*4
	 # a = a*5 {
	movl	-4(%rbp), %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	%eax, -4(%rbp)
	 # } a = a*5
	 # a = a*6 {
	movl	-4(%rbp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	movl	%eax, -4(%rbp)
	 # } a = a*6
	 # a = a*7 {
	movl	-4(%rbp), %edx
	movl	%edx, %eax
	sall	$3, %eax
	subl	%edx, %eax
	movl	%eax, -4(%rbp)
	 # } a = a*7
	 # a = a*8 {
	sall	$3, -4(%rbp)
	 # } a = a*8
	 # return 0 {
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
	.section	.note.GNU-stack,"",@progbits
