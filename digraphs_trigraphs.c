// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>

void digraphs(int* a) <%
	printf("digraphs: %d\n", a<:0:>);
%>

void trigraphs(int* a) ??<
	printf("trigraphs: %d\n", a??(0??));
??>

int main(void) {
	int a[1] = {13};
	digraphs(a);
	trigraphs(a);
	exit(EXIT_SUCCESS);
}
