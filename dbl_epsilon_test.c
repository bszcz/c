// Copyright 2012-2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <time.h>

int main(void) {
	const int N = 10;
	double a;
	double b;
	double c;
	double d;

	printf("DBL_EPSILON = %g \n", DBL_EPSILON);

	srand(time(NULL));
	for (int i = 0; i < N; i++) {
		a = (double)rand() / RAND_MAX;
		b = (double)rand() / RAND_MAX;

		c = 0.0;
		c += a;
		c += b;

		d = c;
		d -= b;
		d -= a;

		printf("% 3d : ", i);
		if (d != 0.0) {
			printf("FAIL (d = %+.4f * DBL_EPSILON)\n", d / DBL_EPSILON);
		} else {
			printf("PASS\n");
		}
	}

	return 0;
}
