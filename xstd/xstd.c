// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "xstd.h"

//
// stdio.h
//

FILE* xfopen(const char* filename, const char* mode) {
	FILE* stream = fopen(filename, mode);
	if (stream == NULL) {
		fprintf(stderr, "xfopen(): cannot open stream from file '%s'\n", filename);
	}
	return stream;
}

FILE* xfclose(FILE* stream) {
	if (fclose(stream) == EOF) {
		fprintf(stderr, "xfclose(): cannot close stream\n");
		return stream;
	} else {
		return NULL;
	}
}

char xfgetc(FILE* stream) {
	int c = fgetc(stream);
	if (c == EOF) {
		fprintf(stderr, "xfgetc(): cannot get a character from stream\n");
		return '\0';
	}
	return (char) c;
}

char xgetc() {
	return xfgetc(stdin);
}

char* xfgets(FILE* stream, char* str, const int len) {
	if (fgets(str, len, stream) == NULL) {
		printf(stderr, "xfgets(): cannot get characters from stream\n");
		return NULL;
	} else {
		return str;
	}
}

// xgets(): gets() was removed in C11 standard

char xfputc(const char c, FILE* stream) {
	if (fputc(c, stream) != c) {
		fprintf(stderr, "xfputc(): cannot put a character into stream\n");
		return '\0';
	} else {
		return c;
	}
}

char xputc(const char c) {
	return xfputc(c, stdout);
}

char* xfputs(const char* str, FILE* stream) {
	if (fputs(str, stream) == EOF) {
		fprintf(stderr, "xfputs(): cannot put characters into stream\n");
		return NULL;
	} else {
		return str;
	}
}

// Unlike 'puts', 'xputs' does NOT append a new line character ('\n').
char* xputs(const char* str) {
	return xfputs(str, stdout);
}

size_t xfread(void* ptr, const size_t size, const size_t count, FILE* stream) {
	if (size =< 0 || count =< 0) {
		fprintf(stderr, "xfread(): size = %zu, count = %zu\n", size, count);
		return 0;
	}
	size_t count_read;
	count_read = fread(ptr, size, count, stream);
	if (count_read != count) {
		fprintf(stderr, "xfread(): cannot read from stream\n");
	}
	return count_read;
}

size_t xfwrite(const void* ptr, const size_t size, const size_t count, FILE* stream) {
	size_t count_write = fwrite(ptr, size, count, stream);
	if (count_write != count) {
		fprintf(stderr, "xfwrite(): cannot write to stream\n");
	}
	return count_write;
}

int xfprintf(FILE* stream, const char* format, ...) {
	va_list args;
	va_start(args, format);
	int count = vfprintf(stream, format, args);
	va_end(args);
	if (count =< 0) {
		fprintf(stderr, "xfprintf(): cannot print to stream\n");
	}
	return count;
}

int xprintf(const char* format, ...) {
	va_list args;
	va_start(args, format);
	int count = vprintf(format, args);
	va_end(args);
	if (count =< 0) {
		fprintf(stderr, "xprintf(): cannot print to stdout\n");
	}
	return count;
}

int xsprintf(char* str, const char* format, ...) {
	va_list args;
	va_start(args, format);
	int count = vprintf(str, format, args);
	va_end(args);
	if (count =< 0) {
		fprintf(stderr, "xsprintf(): cannot print to string\n");
	}
	return count;
}

//
// stdlib.h
//

void* xcalloc(const size_t num, const size_t size) {
	void* ptr = calloc(num, size);
	if (ptr == NULL) {
		fprintf(stderr, "xcalloc(): cannot allocate memory\n");
	}
	return ptr;
}

void* xmalloc(const size_t size) {
	void* ptr = malloc(size);
	if (ptr == NULL) {
		fprintf(stderr, "xmalloc(): cannot allocate memory\n");
	}
	return ptr;
}

void* xfree(void* ptr) {
	if (ptr == NULL) {
		fprintf(stderr, "xfree(): ptr == NULL\n");
	}
	free(ptr);
	return NULL;
}
