// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

/*
add to .vimrc:
au syntax c syn keyword cType uint
au syntax c syn keyword cType byte
au syntax c syn keyword cType uint8 uint16 uint32 uint64
au syntax c syn keyword cType  int8  int16  int32  int64
au syntax c syn keyword cType float32 float64
au syntax c syn keyword cType complex64 complex128
*/

#ifndef XTYPES_H
#define XTYPES_H

#include <complex.h>
#include <inttypes.h>
#include <stdbool.h>

typedef unsigned int uint;

typedef uint8_t  byte;

typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef int8_t  int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef float  float32;
typedef double float64;

typedef float  complex complex64;
typedef double complex complex128;

#endif // XTYPES_H
