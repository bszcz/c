// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#ifndef XSTD_H
#define XSTD_H

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

//
// stdio.h
//

FILE* xfopen(const char* filename, const char* mode);

FILE* xfclose(FILE* stream);

char xfgetc(FILE* stream);

char xgetc();

char* xfgets(FILE* stream, char* str, const int len);

// xgets(): gets() was removed in C11 standard

char xfputc(const char c, FILE* stream);

char xputc(const char c);

char* xfputs(const char* str, FILE* stream);

char* xputs(const char* str); // does NOT append a newline character

size_t xfread(void* ptr, const size_t size, const size_t count, FILE* stream);

size_t xfwrite(const void* ptr, const size_t size, const size_t count, FILE* stream);

int xfprintf(FILE* stream, const char* format, ...);

int xprintf(const char* format, ...);

int xsprintf(char* str, const char* format, ...);

//
// stdlib.h
//

void* xcalloc(const size_t num, const size_t size);

void* xmalloc(const size_t size);

void* xfree(void* ptr);

#endif // XSTD_H
