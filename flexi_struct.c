// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>

struct flexi {
	int len;
	int array[]; // must be last member
};

int main(void) {
	// size of struct flexi excludes array[]
	printf("sizeof(int)          = %lu\n", sizeof(int));
	printf("sizeof(struct flexi) = %lu\n", sizeof(struct flexi));

	const int len = 10;
	const int mem_size = sizeof(struct flexi) + len * sizeof(int);
	struct flexi* sf = malloc(mem_size);

	const int i = len - 1;
	sf->array[i] = i;
	printf("sf->array[%d] = %d\n", i, sf->array[i]);

	exit(EXIT_SUCCESS);
}
