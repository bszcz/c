// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// background story: search for "Josephus problem"

#include <stdio.h>
#include <stdlib.h>

struct soldiers {
	int* pos;
	int size;
};

struct soldiers* form_platoon(int size);

void kill_soldier(struct soldiers* platoon, int pos);

void kill_platoon(struct soldiers* platoon, int skip);

int main(void) {
	struct soldiers* platoon = form_platoon(41);
	kill_platoon(platoon, 3);
	free(platoon);
	exit(EXIT_SUCCESS);
}

struct soldiers* form_platoon(int size) {
	struct soldiers* platoon = malloc(sizeof(struct soldiers));
	platoon->size = size;
	platoon->pos = malloc(size * sizeof(int));
	for (int i = 0; i < size; i++) {
		platoon->pos[i] = i + 1;
	}
	return platoon;
}

void kill_soldier(struct soldiers* platoon, int index) {
	for (int i = index; i < platoon->size - 1; i++) {
		platoon->pos[i] = platoon->pos[i + 1];
	}
	platoon->size -= 1;
}

void kill_platoon(struct soldiers* platoon, int skip) {
	printf("killing:");
	int index = 0;
	while (platoon->size > 1) {
		index = (index + skip - 1) % platoon->size; // obiously... (?)
		printf(" %d", platoon->pos[index]);
		kill_soldier(platoon, index);
	}
	printf("\nsurvivor: %d\n", platoon->pos[0]);
}
