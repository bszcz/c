// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// background story: search for "Josephus problem"

#include <stdio.h>
#include <stdlib.h>

struct soldier {
	int pos;
	struct soldier* next;
};

struct soldier* form_platoon(int size);

struct soldier* kill_platoon(struct soldier* current, int skip);

int main(void) {
	struct soldier* last = form_platoon(41);
	last = kill_platoon(last, 3);
	free(last);
	exit(EXIT_SUCCESS);
}

struct soldier* form_platoon(int size) {
	struct soldier* first = malloc(sizeof(struct soldier));
	struct soldier* current = first;
	for (int p = 1; p < size; p++) {
		current->pos = p;
		current->next = malloc(sizeof(struct soldier));
		current = current->next;
	}
	current->pos = size;
	current->next = first;
	return current; // last soldier in the chain
}

struct soldier* kill_platoon(struct soldier* current, int skip) {
	printf("killing:");
	while (current->next != current) {
		for (int s = 0; s < skip - 1; s++) {
			current = current->next;
		}
		printf(" %d", current->next->pos);
		struct soldier* killed = current->next;
		current->next = killed->next;
		free(killed);
	}
	printf("\nsurvivor: %d\n", current->pos);
	return current; // last soldier standing
}
