// Copyright (c) 2012-2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>

int main(void) {
	const int N = 9;
	int start_at_zero[N];
	int* start_at_one = start_at_zero - 1;

	for (int i = 0; i < N; i++) {
		const int mul = 9;
		start_at_zero[i] = mul * i;
	}

	for (int i = 0; i < N; i++) {
		printf("start_at_zero[%d] = %d\n", i, start_at_zero[i]);
	}

	printf("\n");

	for (int i = 1; i <= N; i++) {
		printf("start_at_one[%d] = %d\n", i, start_at_one[i]);
	}

	return 0;
}
