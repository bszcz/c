// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// needs the SSL library, use "-lssl" when compiling

#include <inttypes.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <stdio.h>

void print_hash(const unsigned char* hash, const int hash_len) {
	for (int i = 0; i < hash_len; i++) {
		printf("%02x", hash[i]);
	}
}

int main(void) {
	const int data_len = 1234;
	const int data_size = data_len * sizeof(double) / sizeof(char);
	double data[data_len];
	for (int i = 0; i < data_len; i++) {
		data[i] = 0.123456789 * (double)i;
	}

	unsigned char hash_md5[MD5_DIGEST_LENGTH];
	unsigned char hash_sha1[SHA_DIGEST_LENGTH];
	MD5((const unsigned char*)data, data_size, hash_md5);
	SHA1((const unsigned char*)data, data_size, hash_sha1);

	printf("MD5()  = ");
	print_hash(hash_md5, MD5_DIGEST_LENGTH);
	printf("\n");

	printf("SHA1() = ");
	print_hash(hash_sha1, SHA_DIGEST_LENGTH);
	printf("\n");

	return 0;
}
