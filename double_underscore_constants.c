// Copyright (c) 2011, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	printf("__LINE__ = %d\n",   __LINE__); // source code line no.
	printf("__FILE__ = '%s'\n", __FILE__); // name of source file
	printf("__DATE__ = '%s'\n", __DATE__); // date of compilation
	printf("__TIME__ = '%s'\n", __TIME__); // time of compilation

	printf("__linux__ = ");
#ifdef __linux__
	printf("%d", __linux__);
#endif
	printf("\n");

	printf("__STDC__ = "); // standard C
#ifdef __STDC__
	printf("%d", __STDC__);
#endif
	printf("\n");

	printf("__STDC_VERSION__ = "); // standard C version
#ifdef __STDC_VERSION__
	printf("%ld", __STDC_VERSION__);
#endif
	printf("\n");

	printf("__STDC_HOSTED__ = "); // hosted environment
#ifdef __STDC_HOSTED__
	printf("%d", __STDC_HOSTED__);
#endif
	printf("\n");

	printf("__cplusplus = "); // C++ version (=1 in GNU)
#ifdef __cplusplus
	printf("%d", __cplusplus);
#endif
	printf("\n");

	printf("__OBJC__ = "); // objective C
#ifdef __OBJC__
	printf("%d", __OBJC__);
#endif
	printf("\n");

	printf("__ASSEMBLER__ = ");
#ifdef __ASSEMBLER__
	printf("%d", __ASSEMBLER__);
#endif
	printf("\n");

	printf("__LINE__ = %d\n", __LINE__);
#line 1234 // set line no. to 1234
	printf("__LINE__ = %d\n", __LINE__);

	exit(EXIT_SUCCESS);
}
