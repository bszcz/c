// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "udp_tools.h"
#include <string.h>
#include <unistd.h>

void set_key_state(char* keys, int key_sym, char state);

int main(void) {
	struct UDPsopa sopa = {
		.sop = NULL,
		.pap = NULL,
		.port        = 0, // 0 = pick unused port
		.packet_size = 512,
		.packet_num  = 0,
		.host_port   = 4444
	};
	strcpy(sopa.host_name, "127.0.0.1");
	if (udp_setup(&sopa) != 0) {
		udp_cleanup(&sopa);
		exit(EXIT_FAILURE);
	}

	// additional client setup
	if (SDLNet_ResolveHost(&(sopa.pap->address), sopa.host_name, sopa.host_port) != 0) {
		printf("SDLNet_ResolveHost(&(sopa.pap->address), sopa.host_name, sopa.host_port) != 0\n");
		printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
		udp_cleanup(&sopa);
		exit(EXIT_FAILURE);
	}

	if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0) {
		printf("(SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0");
		udp_cleanup(&sopa);
		exit(EXIT_FAILURE);
	}
	int width = 320;
	int height = 240;
	int depth = 32;
	SDL_Surface* screen = SDL_SetVideoMode(width, height, depth, SDL_HWSURFACE | SDL_DOUBLEBUF);
	if (screen == NULL) {
		printf("SDL_SetVideoMode(width, height, depth, SDL_HWSURFACE | SDL_DOUBLEBUF) == NULL\n");
		udp_cleanup(&sopa);
		SDL_Quit();
		exit(EXIT_FAILURE);
	}


	char keys[6] = "1----"; // player 1, '-'/'_' means key up/down
	for (;;) {
		SDL_Delay(10);
		sprintf((char*)sopa.pap->data, "%s", (char*)keys);
		sopa.pap->len = strlen((char*)sopa.pap->data) + 1;

		const int channel = -1; // -1 = use packet's address as destination
		if (SDLNet_UDP_Send(sopa.sop, channel, sopa.pap) == 0) {
			printf("SDLNet_UDP_Send(sopa.sop, channel, sopa.pap) == 0\n");
			printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
			udp_cleanup(&sopa);
			SDL_Quit();
			exit(EXIT_FAILURE);
		}

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_KEYDOWN:
				set_key_state(keys, event.key.keysym.sym, '_');
				break;

			case SDL_KEYUP:
				set_key_state(keys, event.key.keysym.sym, '-');
				break;

			case SDL_QUIT:
				udp_cleanup(&sopa);
				SDL_Quit();
				exit(EXIT_SUCCESS);
			}
		}
	} /* for(;;) */

	udp_cleanup(&sopa);
	SDL_Quit();
	exit(EXIT_SUCCESS);
}

void set_key_state(char* keys, int key_sym, char state) {
	printf("state = %c, key_name = %s\n", state,  SDL_GetKeyName(key_sym));
	switch (key_sym) {
	case SDLK_LEFT:
		keys[1] = state; // keys[0] is player number
		break;
	case SDLK_RIGHT:
		keys[2] = state;
		break;
	case SDLK_UP:
		keys[3] = state;
		break;
	case SDLK_DOWN:
		keys[4] = state;
		break;
	}
}
