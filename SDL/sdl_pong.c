// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <math.h>
#include <SDL/SDL.h>
#include <stdio.h>
#include <stdlib.h>

const int KEYUP = 0;
const int KEYDOWN = 1;

struct controls {
	int up;
	int down;
};

struct velocity {
	float x;
	float y;
};

struct sprite {
	struct velocity vel;
	SDL_Rect rect;
};

void update_keys(struct controls* keys, const Uint8 event_type, const SDLKey key_sym);

void move_sprites(
	struct sprite* player,
	struct sprite* ball,
	const struct controls* keys,
	const SDL_Surface* screen
);

void game_checks(struct sprite* player, struct sprite* ball, SDL_Surface* screen);

int after_glow(SDL_Surface* screen);

int main(void) {
	const Uint32 init_flags = SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER;
	if (SDL_Init(init_flags) != 0) {
		printf("SDL_Init(init_flags) != 0\n");
		printf("SDL_GetError() = '%s'\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	const int width = 320;
	const int height = 240;
	const int depth = 32; // bits per pixel
	const Uint32 video_mode_flags = SDL_HWSURFACE | SDL_DOUBLEBUF;
	SDL_Surface* screen = SDL_SetVideoMode(width, height, depth, video_mode_flags);
	if (screen == NULL) {
		printf("SDL_SetVideoMode(width, height, depth, video_mode_flags) == NULL\n");
		printf("SDL_GetError() = '%s'\n", SDL_GetError());
		SDL_Quit();
		exit(EXIT_FAILURE);
	}

	struct sprite player = {
		.vel = {4.0f, 4.0f},
		.rect = {10, 10, 10, 50}
	};
	struct sprite ball = {
		.vel = {1.0f, 1.0f},
		.rect = {screen->w/2, screen->h/2, 10, 10}
	};
	struct controls keys = {0, 0};

	for (;;) {
		SDL_Delay(10);
		if (after_glow(screen) != 0) {
			printf("after_glow(screen) != 0\n");
			SDL_Quit();
			exit(EXIT_FAILURE);
		}

		move_sprites(&player, &ball, &keys, screen);
		game_checks(&player, &ball, screen);

		SDL_FillRect(screen, &player.rect, 0xFF00);
		SDL_FillRect(screen, &ball.rect, 0xFFFF);
		SDL_Flip(screen);

		SDL_Event Event;
		while (SDL_PollEvent(&Event)) {
			switch (Event.type) {
			case SDL_KEYUP:
			case SDL_KEYDOWN:
				update_keys(&keys, Event.type, Event.key.keysym.sym);
				break;
			case SDL_QUIT:
				SDL_Quit();
				exit(EXIT_SUCCESS);
			}
		}
	}

	exit(EXIT_SUCCESS);
}

void update_keys(struct controls* keys, const Uint8 event_type, const SDLKey key_sym) {
	int state;
	switch (event_type) {
	case SDL_KEYUP:
		printf("event_type = SDL_KEYUP,   ");
		state = KEYUP;
		break;
	case SDL_KEYDOWN:
		printf("event_type = SDL_KEYDOWN, ");
		state = KEYDOWN;
		break;
	default:
		printf("update_keys(): Unknown event type.\n");
		return;
	}

	switch (key_sym) {
	case SDLK_UP:
		printf("key_sym = SDL_UP\n");
		keys->up = state;
		break;
	case SDLK_DOWN:
		printf("key_sym = SDL_DOWN\n");
		keys->down = state;
		break;
	default:
		printf("update_keys(): Unknown key symbol.\n");
	}
}

void move_sprites(
	struct sprite* player,
	struct sprite* ball,
	const struct controls* keys,
	const SDL_Surface* screen
) {
	player->rect.y += player->vel.y * (keys->down - keys->up);
	if (player->rect.x < 0) {
		player->rect.x = 0;
	}
	if (player->rect.y < 0) {
		player->rect.y = 0;
	}
	if (player->rect.x + player->rect.w > screen->w) {
		player->rect.x = screen->w - player->rect.w;
	}
	if (player->rect.y + player->rect.h > screen->h) {
		player->rect.y = screen->h - player->rect.h;
	}

	ball->rect.x += ball->vel.x;
	ball->rect.y += ball->vel.y;
	if (ball->rect.x < 0) {
		ball->rect.x = 0;
		ball->vel.x *= -1;
	}
	if (ball->rect.y < 0) {
		ball->rect.y = 0;
		ball->vel.y *= -1;
	}
	if (ball->rect.x + ball->rect.w > screen->w) {
		ball->rect.x = screen->w - ball->rect.w;
		ball->vel.x *= -1;
	}
	if (ball->rect.y + ball->rect.h > screen->h) {
		ball->rect.y = screen->h - ball->rect.h;
		ball->vel.y *= -1;
	}

	const float max_ball_vel = 5.0f;
	const float ball_vel_change = 0.01f;
	if (fabs(ball->vel.x) < max_ball_vel) {
		const float vel_x_sign = ball->vel.x / fabs(ball->vel.x);
		const float vel_y_sign = ball->vel.y / fabs(ball->vel.y);
		ball->vel.x += vel_x_sign * ball_vel_change;
		ball->vel.y = vel_y_sign * fabs(ball->vel.x);
	}
}

void game_checks(struct sprite* player, struct sprite* ball, SDL_Surface* screen) {
	if (ball->rect.x <= player->rect.x + player->rect.w) {
		if (ball->rect.y + ball->rect.h >= player->rect.y &&
		    ball->rect.y <= player->rect.y + player->rect.h) {
			if (ball->vel.x < 0) {
				printf("bounce\n");
				ball->vel.x *= -1;
			}
		}
	}
	if (ball->rect.x < player->rect.x) {
		printf("game over\n");
		SDL_FillRect(screen, NULL, 0xFF0000);
		SDL_Flip(screen);
		SDL_Delay(1000);
		SDL_Quit();
		exit(EXIT_SUCCESS);
	}
}

int after_glow(SDL_Surface* screen) {
	if (SDL_MUSTLOCK(screen) == SDL_TRUE) {
		if (SDL_LockSurface(screen) != 0) {
			printf("SDL_LockSurface(screen) != 0\n");
			printf("SDL_GetError() = '%s'\n", SDL_GetError());
			return -1;
		}
	}

	Uint32* pixels = screen->pixels;
	for (int p = 0; p < screen->w * screen->h; p++) {
		Uint8 r;
		Uint8 g;
		Uint8 b;
		SDL_GetRGB(pixels[p], screen->format, &r, &g, &b);

		float glow_delay = 0.5f;
		r *= glow_delay;
		g *= glow_delay;
		b *= glow_delay;

		pixels[p] = SDL_MapRGB(screen->format, r, g, b);
	}

	if (SDL_MUSTLOCK(screen) == SDL_TRUE) {
		SDL_UnlockSurface(screen);
	}

	return 0;
}
