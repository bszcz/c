// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "udp_tools.h"
#include <string.h>
#include <unistd.h>

int udp_reply(struct UDPsopa* sopap);

int main(void) {
	struct UDPsopa sopa = {
		.sop = NULL,
		.pap = NULL,
		.port        = 4444,
		.packet_size = 512,
		.packet_num  = 0,
		.host_port   = 0
	};
	if (udp_setup(&sopa) != 0) {
		udp_cleanup(&sopa);
		exit(EXIT_FAILURE);
	}

	for (;;) {
		switch (SDLNet_UDP_Recv(sopa.sop, sopa.pap)) {
		case -1:
			printf("SDLNet_UDP_Recv(sopa.sop, sopa.pap) == -1\n");
			printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
			udp_cleanup(&sopa);
			exit(EXIT_FAILURE);
		case 0:
			// do nothing
			break;
		case 1:
			sopa.packet_num++;
			udp_print(&sopa);
			if (udp_reply(&sopa) != 0) {
				printf("(udp_reply(&sopa) != 0\n");
				udp_cleanup(&sopa);
				exit(EXIT_FAILURE);
			}
			break;
		}
	}

	udp_cleanup(&sopa);
	exit(EXIT_SUCCESS);
}

int udp_reply(struct UDPsopa* sopap) {
	char reply[512];
	sprintf(reply, "(%d) '%s'", sopap->packet_num, (char*)sopap->pap->data);
	strcpy((char*)sopap->pap->data, reply);
	sopap->pap->len = strlen((char*)sopap->pap->data) + 1;

	const int channel = -1; // -1 = use packet's address as destination
	if (SDLNet_UDP_Send(sopap->sop, channel, sopap->pap) == 0) {
		printf("SDLNet_UDP_Send(sopa.sop, channel, sopa.pap) == 0\n");
		printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
		return -1;
	} else {
		return 0;
	}
}
