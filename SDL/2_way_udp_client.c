// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "udp_tools.h"
#include <string.h>
#include <unistd.h>

int main(void) {
	struct UDPsopa sopa = {
		.sop = NULL,
		.pap = NULL,
		.port        = 0, // 0 = pick unused port
		.packet_size = 512,
		.packet_num  = 0,
		.host_port   = 4444
	};
	strcpy(sopa.host_name, "127.0.0.1");
	if (udp_setup(&sopa) != 0) {
		udp_cleanup(&sopa);
		exit(EXIT_FAILURE);
	}

	// additional client setup
	if (SDLNet_ResolveHost(&(sopa.pap->address), sopa.host_name, sopa.host_port) != 0) {
		printf("SDLNet_ResolveHost(&(sopa.pap->address), sopa.host_name, sopa.host_port) != 0\n");
		printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
		udp_cleanup(&sopa);
		exit(EXIT_FAILURE);
	}

	srand(123);
	for (;;) {
		sleep(1); // 1 second
		sprintf((char*)sopa.pap->data, "rand_num = %d", rand());
		sopa.pap->len = strlen((char*)sopa.pap->data) + 1;

		const int channel = -1; // -1 = use packet's address as destination
		if (SDLNet_UDP_Send(sopa.sop, channel, sopa.pap) == 0) {
			printf("SDLNet_UDP_Send(sopa.sop, channel, sopa.pap) == 0\n");
			printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
			udp_cleanup(&sopa);
			exit(EXIT_FAILURE);
		}

		switch (SDLNet_UDP_Recv(sopa.sop, sopa.pap)) {
		case -1:
			printf("SDLNet_UDP_Recv(sopa.sop, sopa.pap) == -1\n");
			printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
			udp_cleanup(&sopa);
			exit(EXIT_FAILURE);
		case 0:
			printf("SDLNet_UDP_Recv(sopa.sop, sopa.pap) == 0 // no packets received\n");
			break;
		case 1:
			sopa.packet_num++;
			udp_print(&sopa);
			break;
		}
	} // for(;;)

	udp_cleanup(&sopa);
	exit(EXIT_SUCCESS);
}
