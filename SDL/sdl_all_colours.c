// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// Compile with: c99 sdl_all_colours.c -lSDL

#include <SDL/SDL.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// Return values:
//  0 = success
// -1 = error in SDL_LockSurface(screen)
// -2 = error in SDL_Flip(screen)
int draw_screen(SDL_Surface* screen, const int frame, const int nframes) {
	if (SDL_MUSTLOCK(screen) == SDL_TRUE) {
		if (SDL_LockSurface(screen) != 0) {
			printf("SDL_LockSurface(screen) != 0\n");
			return -1;
		}
	}

	const Uint32 colour_shift = frame * screen->h * screen->w;
	Uint32* pixels = screen->pixels;
	for (Uint32 h = 0, colour = 0; h < screen->h; h++) {
		for (Uint32 w = 0; w < screen->w; w++, colour++) {
			*pixels = colour + colour_shift;
			pixels++;
		}
	}

	if (SDL_MUSTLOCK(screen) == SDL_TRUE) {
		SDL_UnlockSurface(screen);
	}

	if (SDL_Flip(screen) != 0) {
		printf("SDL_Flip(screen) != 0\n");
		return -2;
	}

	return 0;
}

int main(void) {
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		printf("SDL_Init(SDL_INIT_VIDEO) != 0\n");
		exit(EXIT_FAILURE);
	}

	int width = 256;
	int height = 256;
	int depth = 32; // bits per pixel
	Uint32 flags = SDL_HWSURFACE | SDL_DOUBLEBUF;
	SDL_Surface* screen = SDL_SetVideoMode(width, height, depth, flags);
	if (screen == NULL) {
		printf("SDL_SetVideoMode(width, height, depth, flags) == NULL\n");
		SDL_Quit();
		exit(EXIT_FAILURE);
	}

	int frame = 0;
	bool keep_drawing = true;
	while (keep_drawing) {
		const int nframes = 256;
		if (draw_screen(screen, frame, nframes) != 0) {
			SDL_Quit();
			exit(EXIT_FAILURE);
		}
		frame = (frame + 1) % nframes;
		SDL_Delay(10);

		SDL_Event event;
		while (SDL_PollEvent(&event) != 0) {
			switch (event.type) {
			case SDL_QUIT:
			case SDL_KEYDOWN:
			case SDL_MOUSEBUTTONDOWN:
				keep_drawing = false;
				break;
			}
		}
	}

	SDL_Quit();
	exit(EXIT_SUCCESS);
}
