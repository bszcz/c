// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#pragma once

#include <SDL/SDL_net.h>
#include <SDL/SDL.h>

struct UDPsopa {
	UDPsocket  sop; // UDPsocket = _UDPsocket* (already a pointer)
	UDPpacket* pap;
	int  packet_size;
	int  packet_num;
	int  port;
	int  host_port;
	char host_name[512];
};

int udp_setup(struct UDPsopa* sopap);

void udp_print(struct UDPsopa* sopap);

void udp_cleanup(struct UDPsopa* sopap);
