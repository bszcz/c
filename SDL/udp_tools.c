// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "udp_tools.h"

int udp_setup(struct UDPsopa* sopap) {
	if (SDLNet_Init() != 0) {
		printf("SDLNet_Init() != 0\n");
		printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
		return -1;
	}
	sopap->sop = SDLNet_UDP_Open(sopap->port);
	if (sopap->sop == NULL) {
		printf("SDLNet_UDP_Open(sopap->port) == NULL\n");
		printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
		return -1;
	}
	sopap->pap = SDLNet_AllocPacket(sopap->packet_size);
	if (sopap->pap == NULL) {
		printf("SDLNet_AllocPacket(sopap->packet_size) == NULL\n");
		printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
		return -1;
	}
	return 0;
}

void udp_print(struct UDPsopa* sopap) {
	printf("(%d)\n", sopap->packet_num);
	printf("UDPpacket = {\n");
	printf("\tchannel = %d\n", sopap->pap->channel);
	printf("\tdata = '%s'\n", (char*)sopap->pap->data);
	printf("\tlen = %d\n", sopap->pap->len);
	printf("\tmaxlen = %d\n", sopap->pap->maxlen);
	printf("\tstatus = %d\n", sopap->pap->status);
	printf("\taddress.host = %s\n", SDLNet_ResolveIP(&(sopap->pap->address)));
	printf("\taddress.port = %u\n", sopap->pap->address.port);
	printf("}\n");
}

void udp_cleanup(struct UDPsopa* sopap) {
	SDLNet_UDP_Close(sopap->sop);
	SDLNet_FreePacket(sopap->pap);
	sopap->sop = NULL;
	sopap->pap = NULL;
	SDLNet_Quit();
}
