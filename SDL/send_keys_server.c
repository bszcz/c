// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "udp_tools.h"
#include <string.h>
#include <unistd.h>

int main(void) {
	struct UDPsopa sopa = {
		.sop = NULL,
		.pap = NULL,
		.port        = 4444,
		.packet_size = 512,
		.packet_num  = 0,
		.host_port   = 0
	};
	if (udp_setup(&sopa) != 0) {
		udp_cleanup(&sopa);
		exit(EXIT_FAILURE);
	}

	for (;;) {
		switch (SDLNet_UDP_Recv(sopa.sop, sopa.pap)) {
		case -1:
			printf("SDLNet_UDP_Recv(sopa.sop, sopa.pap) == -1\n");
			printf("SDLNet_GetError() = '%s'\n", SDLNet_GetError());
			udp_cleanup(&sopa);
			exit(EXIT_FAILURE);
		case 0:
			// do nothing
			break;
		case 1:
			sopa.packet_num++;
			udp_print(&sopa);
			break;
		}
	}

	udp_cleanup(&sopa);
	exit(EXIT_SUCCESS);
}
