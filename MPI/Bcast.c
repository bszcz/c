// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

void assertz(char* msg, int err) {
	if (err) {
		printf("ERROR: %s\n", msg);
		exit(EXIT_FAILURE);
	}
}

int main(int argc, char** argv) {
	int rank;
	assertz("MPI_Init", MPI_Init(&argc, &argv));
	assertz("MPI_Comm_rank", MPI_Comm_rank(MPI_COMM_WORLD, &rank));

	const int data_size = 2;
	int data[data_size];
	const int master_rank = 0;
	if (rank == master_rank) {
		data[0] = 11;
		data[1] = 22;
	}
	MPI_Bcast(&data, data_size, MPI_INT, master_rank, MPI_COMM_WORLD);
	printf("rank = %d : data[] = {%d, %d}\n", rank, data[0], data[1]);

	assertz("MPI_Finalize", MPI_Finalize());
	exit(EXIT_SUCCESS);
}
