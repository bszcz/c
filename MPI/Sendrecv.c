// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

void assertz(char* msg, int err) {
	if (err) {
		printf("ERROR: %s\n", msg);
		exit(EXIT_FAILURE);
	}
}

int main(int argc, char** argv) {
	int rank;
	assertz("MPI_Init", MPI_Init(&argc, &argv));
	assertz("MPI_Comm_rank", MPI_Comm_rank(MPI_COMM_WORLD, &rank));

	const int data_size = 2;
	int data_send[data_size];
	int data_recv[data_size];
	const int tag_1 = 12345;
	const int tag_2 = 67890;
	const int jack = 0;
	const int jill = 1;
	MPI_Status stat;
	if (rank == jack) {
		data_send[0] = 11;
		data_send[1] = 12;

		MPI_Sendrecv(
			&data_send, data_size, MPI_INT, jill, tag_1,
			&data_recv, data_size, MPI_INT, jill, tag_2,
			MPI_COMM_WORLD, &stat
		);
	}
	if (rank == jill) {
		data_send[0] = 21;
		data_send[1] = 22;

		MPI_Sendrecv(
			&data_send, data_size, MPI_INT, jack, tag_2,
			&data_recv, data_size, MPI_INT, jack, tag_1,
			MPI_COMM_WORLD, &stat
		);
	}
	if (rank < 2) {
		printf("rank = %d : data_send[] = {%d, %d} \n",
		       rank, data_send[0], data_send[1]);
		printf("rank = %d : data_recv[] = {%d, %d} \n",
		       rank, data_recv[0], data_recv[1]);
	}

	assertz("MPI_Finalize", MPI_Finalize());
	exit(EXIT_SUCCESS);
}
