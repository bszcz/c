// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

void assertz(char* msg, int err) {
	if (err) {
		printf("ERROR: %s\n", msg);
		exit(EXIT_FAILURE);
	}
}

int main(int argc, char** argv) {
	int rank;

	assertz("MPI_Init", MPI_Init(&argc, &argv));
	assertz("MPI_Comm_rank", MPI_Comm_rank(MPI_COMM_WORLD, &rank));

	printf("Hello World from rank = %d\n", rank);
	assertz("MPI_Barrier", MPI_Barrier(MPI_COMM_WORLD));
	if (rank == 0) {
		printf("Hello World from rank = %d (again)\n", rank);
	}

	assertz("MPI_Finalize", MPI_Finalize());
	exit(EXIT_SUCCESS);
}
