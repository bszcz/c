// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

void assertz(char* msg, int err) {
	if (err) {
		printf("ERROR: %s\n", msg);
		exit(EXIT_FAILURE);
	}
}


int main(int argc, char** argv) {
	int rank;
	int world_size;
	assertz("MPI_Init", MPI_Init(&argc, &argv));
	assertz("MPI_Comm_rank", MPI_Comm_rank(MPI_COMM_WORLD, &rank));
	assertz("MPI_Comm_size", MPI_Comm_size(MPI_COMM_WORLD, &world_size));

	const int data_size = 2;
	int data[data_size];
	const int master_rank = 0;
	const int tag = 12345;
	MPI_Status stat;
	if (rank == master_rank) {
		data[0] = 1;
		data[1] = 2;

		for (int r = 1; r < world_size; r++) {
			MPI_Ssend(&data, data_size, MPI_INT, r, tag, MPI_COMM_WORLD);
		}

		data[0] = 11;
		data[1] = 22;
	} else {
		MPI_Recv(&data, data_size, MPI_INT, master_rank, tag, MPI_COMM_WORLD, &stat);

		int count;
		assertz("MPI_Get_count", MPI_Get_count(&stat, MPI_INT, &count));
		assertz("data_size == count", data_size - count); // == 0 ??

		/*
		printf("stat.MPI_SOURCE = %d\n", stat.MPI_SOURCE);
		printf("stat.MPI_TAG    = %d\n", stat.MPI_TAG);
		printf("stat.MPI_ERROR  = %d\n", stat.MPI_ERROR);
		*/
	}
	printf("rank = %d : data[] = {%d, %d}\n", rank, data[0], data[1]);

	assertz("MPI_Finalize", MPI_Finalize());
	exit(EXIT_SUCCESS);
}
