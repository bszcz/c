// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

void assertz(char* msg, int err) {
	if (err) {
		printf("ERROR: %s\n", msg);
		exit(EXIT_FAILURE);
	}
}

int main(int argc, char** argv) {
	int rank;
	assertz("MPI_Init", MPI_Init(&argc, &argv));
	assertz("MPI_Comm_rank", MPI_Comm_rank(MPI_COMM_WORLD, &rank));

	double wtime_start = MPI_Wtime();
	double sum = 0.0;
	int max = 1e6 * (1 + rank);
	for (int i = 0; i < max; i++) {
		double x = 0.1234;
		sum += exp(x * (double)i / (double)max);
	}
	double wtime_stop = MPI_Wtime();

	printf("rank = %d : sum = %16.6lf, max = % 10ld, wtime = % 12.9lf\n",
	       rank, sum, max, wtime_stop - wtime_start);

	assertz("MPI_Finalize", MPI_Finalize());
	exit(EXIT_SUCCESS);
}
