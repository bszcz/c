// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <limits.h>
#include <stdio.h>

int main(void) {
	printf("char  : % 20d % 20d\n", CHAR_MIN, CHAR_MAX);
	printf("short : % 20d % 20d\n", SHRT_MIN, SHRT_MAX);
	printf("int   : % 20d % 20d\n", INT_MIN, INT_MAX);
	printf("long  : % 20ld % 20ld\n", LONG_MIN, LONG_MAX);
	printf("llong : % 20lld % 20lld\n", LLONG_MIN, LLONG_MAX);
	return 0;
}
