// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#define OUT // label output variables
#define IN  // label  input variables

#include <stdio.h>
#include <stdlib.h>

void square_and_cube(
	OUT int* square, int* cube, // output variables
	IN  const int n             //  input variables
) {
	*square = n * n;
	*cube = n * n * n;
}

int main(void) {
	const int number = -2;
	int square = 0;
	int cube = 0;

	square_and_cube(&square, &cube, number);

	printf("number = %2d\n", number);
	printf("square = %2d\n", square);
	printf("cube   = %2d\n", cube);

	exit(EXIT_SUCCESS);
}
