// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <iso646.h> // and, or, not, ...
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	const bool flag1 = true;
	const bool flag2 = false;
	const bool flag3 = false;

	if ((flag1 || flag2) && (!flag3)) {
		printf("OK.\n");
	}

	if ((flag1 or flag2) and (not flag3)) {
		printf("OK.\n");
	}

	exit(EXIT_SUCCESS);
}
