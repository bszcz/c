// Copyright (c) 2012 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <fcntl.h>

int main() {
	printf("flag     hex  dec  \n");
	printf("------------------ \n");
	printf("O_RDONLY %04x %04d \n", O_RDONLY, O_RDONLY);
	printf("O_WRONLY %04x %04d \n", O_WRONLY, O_WRONLY);
	printf("O_RDWR   %04x %04d \n", O_RDWR,   O_RDWR);
	printf("O_CREAT  %04x %04d \n", O_CREAT,  O_CREAT);
	printf("O_APPEND %04x %04d \n", O_APPEND, O_APPEND);
	return 0;
}
