## Gillespie 1976

Applying the Gillespie algorithm to the system of reactions (33) from his 1976 paper.

              c_1
        X <___---> Y   (33a)
           c_2
    
              c_3
       2X <___---> Z   (33b)
           c_4
    
              c_5
    W + X <___---> 2X  (33c)
           c_6

The original program was written in FORTRAN with flow control based on 10 GOTO statements and no subroutines -- the software solution still very popular in academic circles. Then again, there was actually source code published in the paper which allows for transparency and reproducibility of the research. Some call it "the scientific method". It is yet to become popular.

### COPYRIGHT / LICENSE

The MIT License (MIT)
