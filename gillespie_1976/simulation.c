// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// program to simulate reactions in system (33)

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NREACT 6 // number of reactions in system (33)

struct sys33 {
	double rates[NREACT];
	double props[NREACT]; // propensities
	double props_tot; // total propensity
	double W, X, Y, Z; // chemical species
};

void calc_props(struct sys33* sys) {
	sys->props[0] = sys->rates[0] * sys->X;
	sys->props[1] = sys->rates[1] * sys->Y;
	sys->props[2] = sys->rates[2] * sys->X * (sys->X - 1.0) / 2.0;
	sys->props[3] = sys->rates[3] * sys->Z;
	sys->props[4] = sys->rates[4] * sys->W * sys->X;
	sys->props[5] = sys->rates[5] * sys->X * (sys->X - 1.0) / 2.0;
	sys->props_tot = 0.0;
	for (int i = 0; i < NREACT; i++) {
		sys->props_tot += sys->props[i];
	}
}

struct sys33* init_system(char** argv) {
	struct sys33* sys = calloc(1, sizeof(struct sys33));
	if (sys == NULL) {
		printf("init_system() error: cannot allocate memory\n");
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < NREACT; i++) {
		sys->rates[i] = strtod(argv[i + 1], NULL);
	}
	sys->W = strtod(argv[NREACT + 1], NULL);
	sys->X = strtod(argv[NREACT + 2], NULL);
	sys->Y = strtod(argv[NREACT + 3], NULL);
	sys->Z = strtod(argv[NREACT + 4], NULL);
	calc_props(sys);
	return sys;
}

void print_system(double t, struct sys33* sys) {
	printf("%.4f %.0f %.0f %.0f %.0f\n", t, sys->W, sys->X, sys->Y, sys->Z);
}

int react_search(struct sys33* sys) {
	double rnd = (double)rand() / RAND_MAX; // interval: [0.0, 1.0)
	double props_sum = 0.0;
	for (int i = 0; i < NREACT; i++) {
		props_sum += sys->props[i];
		if (props_sum >= rnd * sys->props_tot) {
			return i;
		}
	}
	return (NREACT - 1);
}

void react_execute(struct sys33* sys, const int react) {
	switch (react) {
	case 0:
		sys->X -= 1;
		sys->Y += 1;
		break;
	case 1:
		sys->X += 1;
		sys->Y -= 1;
		break;
	case 2:
		sys->X -= 2;
		sys->Z += 1;
		break;
	case 3:
		sys->X += 2;
		sys->Z -= 1;
		break;
	case 4:
		sys->X += 1;
		sys->W -= 1;
		break;
	case 5:
		sys->X -= 1;
		sys->W += 1;
		break;
	default:
		printf("react_execute() error: invalid reaction\n");
	}
}

double react_system(struct sys33* sys) {
	double rnd = 1.0 - ((double)rand() / RAND_MAX); // interval: (0.0, 1.0]
	double dt = (-1.0) * log(rnd) / sys->props_tot;
	int next_react = react_search(sys);
	react_execute(sys, next_react);
	calc_props(sys);
	return dt;
}

int main(int argc, char** argv) {
	if (argc != (1 + NREACT + 4 + 2)) {
		printf("usage: %s "                     // argc = 1
		       "<c1> <c2> <c3> <c4> <c5> <c6> " // + NREACT
		       "<W> <X> <Y> <Z> "               // + 4
		       "<tint> <tmax>\n", argv[0]);     // + 2
		exit(EXIT_FAILURE);
	}
	struct sys33* sys = init_system(argv);

	double tint = strtod(argv[NREACT + 4 + 1], NULL); // time inteval between outputs
	double tmax = strtod(argv[NREACT + 4 + 2], NULL); // maximum time of simulation
	double tout = tint; // time of next output
	double t = 0.0; // time
	print_system(t, sys);

	srand(time(NULL));
	while (t <= tmax) {
		t += react_system(sys);
		if (t >= tout) {
			print_system(t, sys);
			tout += tint;
		}
	}

	free(sys);
	exit(EXIT_SUCCESS);
}
