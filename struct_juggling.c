// Nopyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct data {
	long counter;
	double numbers[32]; // 256 bytes
};

void inc_s(struct data d) {
	d.counter++;
}

void inc_p(struct data* d) {
	d->counter++;
}

int main(void) {
	const long N = 1000 * 1000 * 1000;
	time_t t1 = 0;
	time_t t2 = 0;

	struct data* dat_p = calloc(1, sizeof(struct data));
	time(&t1);
	for (long i = 0; i < N; i++) {
		inc_p(dat_p);
	}
	time(&t2);
	printf("pointer only = %.1lf seconds\n", difftime(t2, t1));

	struct data dat_s = {.counter = 0};
	time(&t1);
	for (long i = 0; i < N; i++) {
		inc_s(dat_s);
	}
	time(&t2);
	printf("whole struct = %.1lf seconds\n", difftime(t2, t1));

	return 0;
}
