// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>

#define test_r(r) test_r_(#r, r)
void test_r_(char* r_name, int (*r_func)(int));

int r1(int i); // :-(
int r2(int i); // :-|
int r3(int i); // :-)

#define test_d(d) test_d_(#d, d)
void test_d_(char* d_name, int (*r_func)(int));

int d1(int m); // :-(
int d2(int m); // :-)

int main(void) {
	test_r(r1);
	test_r(r2);
	test_r(r3);

	test_d(d1);
	test_d(d2);

	exit(EXIT_SUCCESS);
}

void test_r_(char* r_name, int (*r_func)(int)) {
	printf("testing %s():\n", r_name);
	for (int i = 0; i < 3; i++) {
		printf("\t%s(%d) = %d\n", r_name, i, (*r_func)(i));
	}
	return;
}

int r1(int i) {
	return (2 - i) * (1 + (3 * i)) / 2;
}

int r2(int i) {
	return (i + 1) % 3;
}

int r3(int i) {
	i++;
	if (i == 3) {
		i = 0;
	}
	return i;
}

void test_d_(char* d_name, int (*d_func)(int)) {
	printf("testing %s():\n", d_name);
	for (int m = 1; m <= 12; m++) {
		printf("\t%s(%2d) = %d\n", d_name, m, (*d_func)(m));
	}
	return;
}

int d1(int m) {
	return 28 + (m + (m / 8)) % 2 + 2 % m + 2 * (1 / m);
}

int d2(int m) {
	switch (m) {
	case  2:
		return 28;
	case  4:
	case  6:
	case  9:
	case 11:
		return 30;
	default:
		return 31;
	}
}
