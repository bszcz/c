set grid
set xlabel "skip"
set logscale x 2
set logscale y
set key top right
plot \
"cache_line.txt" using 1:2 title "time [seconds]", \
"cache_line.txt" using 1:3 title "efficiency"
pause -1
