// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

/*
fast_sloppy_checksum.c outputs every (2^p)th byte from a file
which can then be used to calculate an "approximate" checksum

obviously, if two files have identical "approximate" checksums
they are NOT guarateed to be identical, but they are defintely
NOT identical if their "approximate" checksum are different...
...provided that the checksum is not prone to "collision" ;-)
*/

#define ERRNO_ARGC 1
#define ERRNO_POW  2
#define ERRNO_FILE 3
#define ERRNO_SIZE 4
#define ERRNO_SEEK 5

#include <stdlib.h>
#include <stdio.h>

static const char ERRMSG_POW[]  = "ERROR: POWER should be greater than zero.\n";
static const char ERRMSG_FILE[] = "ERROR: Could not open the FILE.\n";
static const char ERRMSG_ARGC[] =
	"SYNOPSIS\n"
	"    %s [POWER] [FILE]\n"
	"\n"
	"DESCRIPTION\n"
	"    print every (2^POWER)th byte from FILE to STDOUT\n";

int main(int argc, char** argv) {
	if (argc != 3) {
		printf(ERRMSG_ARGC, argv[0]);
		exit(ERRNO_ARGC);
	}

	long power = strtoul(argv[1], NULL, 10);
	if (power == 0) {
		printf(ERRMSG_POW);
		exit(ERRNO_POW);
	}
	long file_jump = 1 << power; // = 2^power

	FILE* file_ptr = fopen(argv[2], "rb");
	if (file_ptr == NULL) {
		printf(ERRMSG_FILE);
		exit(ERRNO_FILE);
	}

	if (fseek(file_ptr, 0, SEEK_END) != 0) {
		exit(ERRNO_SEEK);
	}
	long file_size = ftell(file_ptr);
	if (file_size == -1L) {
		exit(ERRNO_SIZE);
	}
	rewind(file_ptr);

	for (long file_pos = 0; file_pos < file_size; file_pos += file_jump) {
		putc(getc(file_ptr), stdout);
		if (fseek(file_ptr, file_jump - 1, SEEK_CUR) != 0) {
			exit(ERRNO_SEEK);
		}
	}

	fclose(file_ptr);
	exit(EXIT_SUCCESS);
}
