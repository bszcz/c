// Copyright (c) 2012-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
	const int ndata = 64 * 1024 * 1024;
	const int nloops = 1024;
	const int max_skip = 4096;

	int* data = calloc(ndata, sizeof(int));
	if (data == NULL) {
		exit(EXIT_FAILURE);
	}

	printf("# skip time efficiency\n");

	for (int skip = 1; skip <= max_skip; skip *= 2) {
		clock_t t = clock();
		for (int nl = 0; nl < nloops; nl++) {
			for (int nd = 0; nd < ndata; nd += skip) {
				data[nd]++;
			}
		}
		t = clock() - t;

		clock_t t1;
		if (skip == 1) {
			t1 = t;
		}
		printf("%d %lf %lf\n", skip, (double)t / CLOCKS_PER_SEC, (double)t1 / t / skip);
	}

	free(data);
	exit(EXIT_SUCCESS);
}
