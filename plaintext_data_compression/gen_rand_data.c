// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	const int CHARS_IN_DOUBLE = sizeof(double) / sizeof(char);
	const int N = 10e6 / CHARS_IN_DOUBLE; // 100MB of raw data

	FILE* fileDoubleNorm = fopen("randomDoubleNorm.txt", "wb");
	FILE* fileDoubleFull = fopen("randomDoubleFull.txt", "wb");
	FILE* fileLongLong = fopen("randomLongLong.txt", "wb");
	FILE* fileBinary = fopen("randomBinary.dat", "wb");

	union {
		char chars[CHARS_IN_DOUBLE];
		long long as_llong;
		double as_double;
	} r;
	srand(666);
	for (long n = 0; n < N; n++) {
		for (int c = 0; c < CHARS_IN_DOUBLE; c++) {
			r.chars[c] = rand() % CHAR_MAX;
		}
		// %.17lg --> 17 digits after the decimal point preserves double precision
		fprintf(fileDoubleNorm, "%.17lg\n", (double)r.as_llong / (double)LLONG_MAX);
		fprintf(fileDoubleFull, "%.17lg\n", r.as_double);
		fprintf(fileLongLong, "%lld\n", r.as_llong);
		fwrite(r.chars, sizeof(char), CHARS_IN_DOUBLE, fileBinary);
	}

	fclose(fileDoubleNorm);
	fclose(fileDoubleFull);
	fclose(fileLongLong);
	fclose(fileBinary);

	exit(EXIT_SUCCESS);
}
