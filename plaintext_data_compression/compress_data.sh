# Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
# This program is free software under the MIT license.

for name in $(\ls random*.txt); do
	echo "name: '$name'"
	stem=${name%.txt}
	for level in best fast; do
		echo " level: '$level'"
		echo "  program: 'gzip'"
		time gzip  -c --$level $name > $stem-$level.gz
		echo "  program: 'bzip2'"
		time bzip2 -c --$level $name > $stem-$level.bz2
	done
done
