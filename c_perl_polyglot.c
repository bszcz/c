#define Copyright (c) 2012 Bartosz Szczesny <bszcz@bszcz.org>
#define This program is free software under the MIT license.

#include <stdlib.h>
#include <stdio.h>

#define $ /* replace with empty string */
#define if($x) int main(int argc, char *ARGV[])
#define $start 1
#if PERL
sub atoi { $_[0] }
$ argc = @ARGV;
$ start = 0;
$ x = 1;
#endif

if ($x) {
	int $ sum;
	int $ i;
	$ sum = 0;
	for ($ i = $start; $ i < $ argc; $ i++) {
		$ sum += atoi($ ARGV[$ i]);
	}
	printf("%d\n", $ sum);

	exit(0);
}
