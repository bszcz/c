// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <unistd.h>

int main(void) {
	const int nfds = 1; // watch nfds file descriptors
	int epfd = epoll_create(nfds);
	if (epfd == -1) {
		printf("epoll_create: cannot create an epoll file descriptor\n");
		exit(EXIT_FAILURE);
	}

	const int nevents = 1;
	struct epoll_event events[nevents];
	events[0].data.fd = STDIN_FILENO;
	events[0].events = EPOLLIN; // can read without blocking

	if (epoll_ctl(epfd, EPOLL_CTL_ADD, STDIN_FILENO, &events[0])) {
		printf("epoll_ctl: cannot control an epoll file descriptor\n");
		close(epfd);
		exit(EXIT_FAILURE);
	}

	for (int i = 10; i > 0; i--) {
		printf("%2d: Please press Enter...\n", i);

		const int timeout = 1000; // milliseconds
		int npolled = epoll_wait(epfd, events, nevents, timeout);

		if (npolled == -1) {
			printf("epoll_wait: cannot poll events\n");
			close(epfd);
			exit(EXIT_FAILURE);
		}

		if (npolled > 0) {
			printf("%2d: Splendid! You are a gentleman and a scholar.\n", i);
			while (fgetc(stdin) != '\n'); // clear stdin
		}
	}

	close(epfd);
	exit(EXIT_SUCCESS);
}
