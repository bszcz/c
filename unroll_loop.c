// Copyright (c) 2012-2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

/*
	GCC optimisation flag
	with time in seconds:

		-O0	-O3
	--------------------
	normal	31.7	10.5
	unroll	20.9	 3.5
*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	const long N = 1e10;
	const long M = 13;

	double sum = 0.0;
	double sums[4] = {0.0};

	if (argc == 1) {
		printf("this is how I roll...\n");
		for (long i = 0; i < N; i++) {
			sum += M;
		}
	} else {
		printf("this is how I unroll...\n");
		for (long i = 0; i < N; i += 4) {
			sums[0] += M;
			sums[1] += M;
			sums[2] += M;
			sums[3] += M;
		}
		for (int i = 0; i < 4; i++) {
			sum += sums[i];
		}
	}

	printf("sum = %e\n", sum);
	return 0;
}
