// Copyright (c) 2014-2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>

#define fori(n) for (int i = 0; i < (n); i++)
#define forj(n) for (int j = 0; j < (n); j++)
// Be careful with defining fork()!
// Use loopi, loopj, loopk instead.

#define log(msg, ...) printf("[%s:%s:%d] "msg, __FILE__, __func__, __LINE__, __VA_ARGS__)

// print variables with their names
#define putl(l) printf(#l " = %ld\n", (long)(l))
#define putd(d) printf(#d " = %f\n", (double)(d))

// generic print (since C11)
#define gen_fmt(x) _Generic((x), \
	const int: "%d",\
	int: "%d",\
	const long: "%d",\
	long: "%d",\
	const double: "%f",\
	double: "%f",\
	const float: "%f",\
	float: "%f")
#define putx(x) printf(#x" = "); printf(gen_fmt(x), x); printf("\n");

void some_func(int i) {
	log("i = %d\n", i);
}

// unnamed arrays
#define vec(...) ((const double[]){__VA_ARGS__})

void print_vec2(const double* v2) {
	printf("v2 = {%f, %f}\n", v2[0], v2[1]);
}

void print_vec3(const double* v3) {
	printf("v3 = {%f, %f, %f}\n", v3[0], v3[1], v3[2]);
}

// default values
struct player {
	char* name;
	int lives;
};

#define player(name_, ...) (struct player) {.name = name_, .lives = 3, __VA_ARGS__}

void print_player(const struct player p) {
	printf("player = {name = %s, lives = %d}\n", p.name, p.lives);
}

int main(void) {
	const int max = 3;
	fori(max) {
		forj(max) {
			printf("(%d, %d) ", i, j);
		}
		printf("\n");
	}
	printf("\n");

	some_func(max);
	printf("\n");

	const int i = 10;
	const float f = 1.0f;
	putl(i);
	putd(f);
	putl(10); // still works, sort of...

	putx(i);
	putx(f);

	print_vec2(vec(1.0, 2.0));
	print_vec3(vec(1.0, 2.0, 3.0));

	struct player p1 = player("Mario");
	struct player p2 = player("Luigi", .lives = 2); // -Winitializer-overrides warning
	print_player(p1);
	print_player(p2);
}
