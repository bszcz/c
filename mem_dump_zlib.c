// Copyright (c) 2012-2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>

int main(void) {
	const int N = 10;
	int arr[N];
	for (int i = 0; i < N; i++) {
		const int mul = 9;
		arr[i] = mul * i;
	}

	gzFile file = gzopen("mem.dump.gz", "wb9");
	gzwrite(file, arr, N * sizeof(int));
	gzclose(file);

	for (int i = 0; i < N; i++) {
		arr[i] = 0;
	}

	file = gzopen("mem.dump.gz", "rb");
	gzread(file, arr, N * sizeof(int));
	gzclose(file);

	for (int i = 0; i < N; i++) {
		printf("arr[%d] = %d\n", i, arr[i]);
	}

	return 0;
}
