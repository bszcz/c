// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>

void is_digit(int i) {
	switch (i) {
	case 0 ... 9: // GCC extension
		printf("%d is a digit\n", i);
		break;
	default:
		printf("%d is not a digit\n", i);
	}
}

void is_uppercase(char c) {
	switch (c) {
	case 'A' ... 'Z': // GCC extension
		printf("'%c' is upper case\n", c);
		break;
	default:
		printf("'%c' is not upper case\n", c);
	}
}

int main(void) {
	is_digit(7);
	is_digit(13);

	is_uppercase('C');
	is_uppercase('x');

	exit(EXIT_SUCCESS);
}
