// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct bit_date {
	int32_t year : 6;    // 1968 to 2031 (2000-32 to 2000+31)
	uint32_t month : 4;  // 1 to 12
	uint32_t day : 5;    // 1 to 31
	uint32_t hour : 5;   // 0 to 23
	uint32_t minute : 6; // 0 to 59
	uint32_t second : 6; // 0 to 59
};

void bit_date_print(struct bit_date bd) {
	int year = bd.year + 2000;
	printf("%d/%02d/%02d %02d:%02d:%02d\n",
	       year, bd.month, bd.day, bd.hour, bd.minute, bd.second);
}

void bit_date_set(
	struct bit_date* bd,
	int year, int month,  int day,
	int hour, int minute, int second
) {
	// should check values here...
	bd->year = year - 2000;
	bd->month = month;
	bd->day = day;
	bd->hour = hour;
	bd->minute = minute;
	bd->second = second;
}

int main(void) {
	struct bit_date bd;
	printf("sizeof(struct bit_date) = %lu\n", sizeof(struct bit_date));

	bit_date_set(&bd, 1969, 7, 20, 20, 18, 0);
	bit_date_print(bd);

	bit_date_set(&bd, 2031, 12, 31, 0, 0, 0);
	bit_date_print(bd);

	// Y2K32 !!!
	bit_date_set(&bd, 2032, 1, 1, 0, 0, 0);
	bit_date_print(bd);

	exit(EXIT_SUCCESS);
}
