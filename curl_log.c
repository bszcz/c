// Copyright (c) 2012 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <curl/curl.h>

size_t write_func(void* ptr, size_t size, size_t nmemb, void* stream) {
	printf("ptr    = %p\n", ptr);
	printf("size   = %lu\n", (unsigned long)size);
	printf("nmemb  = %lu\n", (unsigned long)nmemb);
	printf("stream = %p\n", stream);
	printf("body   = \n");

	char* c = (char*)ptr;
	for (long i = 0; i < size * nmemb; i++) {
		putchar(c[i]);
	}
	putchar('\n');

	return size * nmemb;
}

int main(void) {
	char log_url[1024] = "http://127.0.0.1/log.php?log=";
	char log_txt[] = "ErrorLog No127 `¬!\"£$%^&*()-_=+[{]};:'@#~,<.>/?\\|";
	char err_buf[CURL_ERROR_SIZE];
	char* log_esc;
	CURL* curlp;

	curlp = curl_easy_init();
	if (curlp == NULL) {
		return 1;
	}

	log_esc = curl_easy_escape(curlp, log_txt, 0); // len = 0 - use strlen()
	if (log_esc == NULL) {
		return 2;
	}

	printf("log_url = %s\n", log_url);
	printf("log_txt = %s\n", log_txt);
	printf("log_esc = %s\n", log_esc);
	sprintf(log_url, "%s%s", log_url, log_esc);
	printf("log_url = %s\n", log_url);

	if (0 != curl_easy_setopt(curlp, CURLOPT_ERRORBUFFER, err_buf)) {
		return 3;
	}
	if (0 != curl_easy_setopt(curlp, CURLOPT_URL, log_url)) {
		return 4;
	}
	if (0 != curl_easy_setopt(curlp, CURLOPT_WRITEFUNCTION, write_func)) {
		return 5;
	}
	if (0 != curl_easy_perform(curlp)) {
		printf("err_buf = %s\n", err_buf);
		return 6;
	}

	curl_free(log_esc);
	curl_easy_cleanup(curlp);

	return 0;
}
