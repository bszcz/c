// Copyright (c) 2012-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

struct channel {
	int read;
	int write;
};

struct channel new_channel() {
	int p[2];
	pipe(p);
	struct channel c = {
		.read  = p[0],
		.write = p[1]
	};
	return c;
}

int main(void) {
	char* msg = "Sent through a pipe.";
	const int BUF_SIZE = 64;
	char buf[BUF_SIZE];

	struct channel chan = new_channel();
	printf("chan = {.read = %d, .write = %d}\n", chan.read, chan.write);

	int is_parent = fork();
	if (is_parent) {
		write(chan.write, msg, BUF_SIZE);
		printf("Parent sent the message    = '%s'\n", msg);
		wait(0);
	} else {
		sleep(1);
		read(chan.read, buf, BUF_SIZE);
		printf("Child received the message = '%s'\n", buf);
	}

	return 0;
}
