// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

struct thread_arg {
	int ndata;
	double* data;
	double psum; // partial sum
};

double hard_sum(double d) {
	double sum = 0.0;
	sum += pow(d, 3.0);
	sum += pow(d, 2.0);
	sum += sqrt(d);
	sum += sin(d);
	sum += cos(d);
	sum += tan(d);
	sum += exp(d);
	sum += log(d);
	return sum;
}

void* thread_func(void* void_arg) {
	struct thread_arg* arg = (struct thread_arg*)void_arg;
	arg->psum = 0.0;
	for (int n = 0; n < arg->ndata; n++) {
		arg->psum += hard_sum(arg->data[n]);
	}
	printf("psum = % 20lf (ndata = % 10d)\n", arg->psum, arg->ndata);
	return NULL;
}

int main(int argc, char** argv) {
	int ndata = 1234321;
	int nthreads = 2;
	if (argc == 3) {
		ndata = strtol(argv[1], NULL, 10);
		nthreads = strtol(argv[2], NULL, 10);
	} else {
		printf("usage: %s <ndata> <nthreads>\n", argv[0]);
	}
	printf("using: ndata = %d, nthreads = %d\n", ndata, nthreads);
	printf("\n");

	double* data = calloc(ndata, sizeof(double));
	if (data == NULL) {
		exit(EXIT_FAILURE);
	}
	srand(13);
	for (int n = 0; n < ndata; n++) {
		data[n] = ((double)rand() + 1.0) / (double)RAND_MAX;
	}

	pthread_t threads[nthreads];
	struct thread_arg args[nthreads];
	int ndata_thread = ndata / nthreads;
	int ndata_topup = ndata - (nthreads * ndata_thread);
	for (int n = 0, shift = 0; n < nthreads; n++) {
		args[n].data = data + shift;
		args[n].ndata = ndata_thread;
		if (ndata_topup > 0) {
			args[n].ndata++;
		}
		ndata_topup--;
		shift += args[n].ndata;
		pthread_create(&threads[n], NULL, thread_func, (void*)&args[n]);
	}

	double sum = 0.0;
	for (int n = 0; n < nthreads; n++) {
		pthread_join(threads[n], NULL);
		sum += args[n].psum;
	}
	printf("------\n");
	printf(" sum = % 20lf (ndata = % 10d)", sum, ndata);

	free(data);
	exit(EXIT_SUCCESS);
}
