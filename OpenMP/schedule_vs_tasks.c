// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void process(const int delay) {
	usleep(delay);
}

int main(void) {
	const int slow_down = 10;
	const int array_size = 1000;
	int* array = malloc(array_size * sizeof(int));
	if (array == NULL) {
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < array_size; i++) {
		array[i] = slow_down * i;
	}

	double t;

	printf("// for \n");
	t = omp_get_wtime();
	#pragma omp parallel
	{
		#pragma omp for
		for (int i = 0; i < array_size; i++) {
			process(array[i]);
		}
	}
	t = omp_get_wtime() - t;
	printf("%d %lf\n", array_size, t);

	printf("// for schedule(dynamic)\n");
	t = omp_get_wtime();
	#pragma omp parallel
	{
		#pragma omp for schedule(dynamic)
		for (int i = 0; i < array_size; i++) {
			process(array[i]);
		}
	}
	t = omp_get_wtime() - t;
	printf("%d %lf\n", array_size, t);

	printf("// single + task\n");
	t = omp_get_wtime();
	#pragma omp parallel
	{
		#pragma omp single
		for (int i = 0; i < array_size; i++) {
			#pragma omp task
			process(array[i]);
		}
	}
	t = omp_get_wtime() - t;
	printf("%d %lf\n", array_size, t);

	free(array);
	exit(EXIT_SUCCESS);
}
