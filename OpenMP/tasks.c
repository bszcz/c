// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <getopt.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct node {
	long rnum;
	struct node* next;
};

struct node* new_node(void) {
	struct node* n = malloc(sizeof(struct node));
	if (n == NULL) {
		fprintf(stderr, "new_node(): cannot allocate memory\n");
	} else {
		n->rnum = -1;
		n->next = NULL;
	}
	return n;
}

struct node* make_list(const int list_size) {
	struct node* first = new_node();
	struct node* current = first;
	current->rnum = rand();
	for (int i = 0; i < list_size - 1; i++) {
		current->next = new_node();
		current = current->next;
		current->rnum = rand();
	}
	return first;
}

void process_node(struct node* n) {
	n->rnum++;
	usleep(1000); // waste time
}

void process_list(struct node* first) {
	#pragma omp parallel
	{
		#pragma omp single
		for (struct node* n = first; n != NULL; n = n->next) {
			#pragma omp task firstprivate(n)
			process_node(n);
		}
	}
}

void print_list(struct node* first, long nodes_to_print) {
	for (struct node* n = first;
	     n != NULL && nodes_to_print > 0;
	     n = n->next, nodes_to_print--) {
		printf("rnum = %ld\n", n->rnum);
	}
}

int main(int argc, char** argv) {
	const int seed = 1234;
	srand(seed);

	long list_size = 1000;
	int max_threads = omp_get_max_threads();
	int num_threads = 1;
	int nodes_to_print = 10;

	int opt;
	while ((opt = getopt(argc, argv, "p:s:t:")) != -1) {
		switch (opt) {
		case 'p':
			nodes_to_print = strtol(optarg, NULL, 10);
			break;
		case 's':
			list_size = strtol(optarg, NULL, 10);
			break;
		case 't':
			num_threads = strtol(optarg, NULL, 10);
			break;
		default:
			printf("usage: %s -p <nodes_to_print> -s <list_size> -t <num_threads>\n", argv[0]);
		}
	}

	omp_set_num_threads(num_threads);
	printf("// list_size = %ld, num_threads = %d, max_threads = %d\n",
	       list_size, num_threads, max_threads);

	struct node* first = make_list(list_size);
	print_list(first, nodes_to_print);
	printf("// processing...\n");
	process_list(first);
	print_list(first, nodes_to_print);

	exit(EXIT_SUCCESS);
}
