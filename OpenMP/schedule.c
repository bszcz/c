// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <getopt.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int* make_array(const long array_size) {
	int* a = malloc(array_size * sizeof(int));
	if (a == NULL) {
		return NULL;
	}
	for (long i = 0; i < array_size; i++) {
		a[i] = 10*i;
	}
	return a;
}

void process(const int delay) {
	usleep(delay);
}

void omp_sched_print(const omp_sched_t omp_sched_kind, const int omp_sched_modifier) {
	printf("// omp_sched_kind = %d, omp_sched_modifier = %d\n",
	       (int)omp_sched_kind, omp_sched_modifier);
}

int main(int argc, char** argv) {
	/* omp.h:
	typedef enum omp_sched_t {
		omp_sched_static  = 1,
		omp_sched_dynamic = 2,
		omp_sched_guided  = 3,
		omp_sched_auto    = 4
	} omp_sched_t;
	*/
	omp_sched_t omp_sched_kind = omp_sched_static;
	int omp_sched_modifier = 0; // value <= 0 sets to default

	long array_size = 1000;

	int opt;
	while ((opt = getopt(argc, argv, "k:m:s:")) != -1) {
		switch (opt) {
		case 'k':
			omp_sched_kind = strtol(optarg, NULL, 10);
			break;
		case 'm':
			omp_sched_modifier = strtol(optarg, NULL, 10);
			break;
		case 's':
			array_size = strtol(optarg, NULL, 10);
			break;
		default:
			printf("usage: %s -k <omp_sched_kind> -m <omp_sched_modifier> -s <array_size>\n", argv[0]);
		}
	}

	int* array = make_array(array_size);
	if (array == NULL) {
		exit(EXIT_FAILURE);
	}

	omp_sched_print(omp_sched_kind, omp_sched_modifier);
	omp_set_schedule(omp_sched_kind, omp_sched_modifier);

	// reset to test if omp_get_schedule() works
	omp_sched_modifier = -1;
	omp_sched_kind = -1;

	double t = omp_get_wtime();
	#pragma omp parallel
	{
		#pragma omp single
		{
			omp_get_schedule(&omp_sched_kind, &omp_sched_modifier);
			omp_sched_print(omp_sched_kind, omp_sched_modifier);
		}
		#pragma omp for schedule(runtime) // "runtime" means set by omp_set_schedule()
		for (int i = 0; i < array_size; i++) {
			process(array[i]);
		}
	}
	printf("%d %d %lf\n", omp_sched_kind, omp_sched_modifier, omp_get_wtime() - t);

	free(array);
	exit(EXIT_SUCCESS);
}
