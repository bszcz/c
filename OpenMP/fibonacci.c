// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <getopt.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int fib(const int fib_start, const int fib_cutoff) {
	if (fib_start < 2) {
		return fib_start;
	}

	int f1;
	int f2;
	if (fib_start > fib_cutoff) {
		#pragma omp task shared(f1)
		{
			f1 = fib(fib_start - 1, fib_cutoff);
		}
		f2 = fib(fib_start - 2, fib_cutoff);
		#pragma omp taskwait
	} else {
		f1 = fib(fib_start - 1, fib_cutoff);
		f2 = fib(fib_start - 2, fib_cutoff);
	}

	return f1 + f2;
}

int main(int argc, char** argv) {
	int fib_start = 30;
	int fib_cutoff = 20;
	int num_threads = 4;

	int opt;
	while ((opt = getopt(argc, argv, "c:hn:t:")) != -1) {
		switch (opt) {
		case 'c':
			fib_cutoff = strtol(optarg, NULL, 10);
			break;
		case 'n':
			fib_start = strtol(optarg, NULL, 10);
			break;
		case 't':
			num_threads = strtol(optarg, NULL, 10);
			break;
		case 'h':
		default:
			printf("usage: %s -c fib_cutoff -n fib_start -t num_threads\n", argv[0]);
			exit(EXIT_SUCCESS);
		}
	}

	double t;
	double t1;
	for (int nth = 1; nth <= num_threads; nth++) {
		omp_set_num_threads(nth);
		#pragma omp parallel
		{
			#pragma omp single
			{
				t = omp_get_wtime();
				fib(fib_start, fib_cutoff);
				t = omp_get_wtime() - t;
				if (nth == 1) {
					t1 = t;
				}
				printf("%d %d %lf %lf\n", fib_cutoff, nth, t1/t, t1/t/nth);
			}
		}
	}

	exit(EXIT_SUCCESS);
}
