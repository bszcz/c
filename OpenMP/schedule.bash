size=1000

for kind in 1 2 3 4; do
	for modifier in 0 1 2 4 8 16 32 64 128 256 512 1024; do
		./a.out -k $kind -m $modifier -s $size | grep -v "^//"
	done
done
