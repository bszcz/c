// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

/*
	$ c99 -lm omp_basic.c
	$ time a.out
	$ c99 -lm omp_basic.c -fopenmp
	$ time a.out
*/

#include <omp.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double HardSum(double d) {
	double sum = 0.0;
	sum += pow(d, 3.0);
	sum += pow(d, 2.0);
	sum += sqrt(d);
	sum += sin(d);
	sum += cos(d);
	sum += tan(d);
	sum += exp(d);
	sum += log(d);
	return sum;
}

int main(void) {
	const long dataSize = 4 * 1024 * 1024;
	float* data = calloc(dataSize, sizeof(float));
	srand(13);
	for (long d = 0; d < dataSize; d++) {
		data[d] = (float)(rand() + 1.0) / (float)RAND_MAX;
	}

	double sum = 0.0;
	#pragma omp parallel for
	for (long d = 0; d < dataSize; d++) {
		#pragma omp atomic
		sum += HardSum(data[d]);
	}
	printf("% 20f \n", sum);

	free(data);
	exit(EXIT_SUCCESS);
}
