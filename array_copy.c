// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>

#define ARRAY_SIZE 16

struct array {
	int item[ARRAY_SIZE];
};

int main(void) {
	struct array fibo; // Fibonacci numbers
	struct array fibo_copy;

	fibo.item[0] = 0;
	fibo.item[1] = 1;

	for (int i = 2; i < ARRAY_SIZE; i++) {
		fibo.item[i] = fibo.item[i - 1] + fibo.item[i - 2];
	}

	fibo_copy = fibo; // copy entire array with assignment statement

	for (int i = 0; i < ARRAY_SIZE; i++) {
		printf("fibo_copy.item[%2d] = %d \n", i, fibo_copy.item[i]);
	}

	return 0;
}
