// Copyright (c) 2011, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

void err(char* msg) {
	printf("ERROR: %s\n", msg);
	exit(EXIT_FAILURE);
}

int main(void) {
	int sock_fd = socket(PF_INET, SOCK_STREAM, 0);
	if (sock_fd == -1) {
		err("socket");
	}
	int yes = 1;
	if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		err("setsocketopt");
	}

	struct sockaddr_in host_addr;
	const int PORT = 10000;
	memset(&host_addr, '\0', sizeof(socklen_t));
	host_addr.sin_family = AF_INET;
	host_addr.sin_port = htons(PORT);
	host_addr.sin_addr.s_addr = 0;

	if (bind(sock_fd, (struct sockaddr*)&host_addr, sizeof(struct sockaddr)) == -1) {
		err("bind");
	}
	const int BACKLOG_Q = 5;
	if (listen(sock_fd, BACKLOG_Q) == -1) {
		err("listen");
	}

	for (;;) {
		struct sockaddr_in client_addr;
		socklen_t sin_size = sizeof(struct sockaddr_in);
		int new_sock_fd = accept(sock_fd, (struct sockaddr*)&client_addr, &sin_size);
		if (new_sock_fd == -1) {
			err("accept");
		}

		printf("SERV: connection from client %s:%d\n",
		       inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
		send(new_sock_fd, "Listening...\n", 13, 0); // 13 = len("Listening...\n")

		for (;;) {
			const int BUF_SZ = 128;
			char buffer[BUF_SZ + 1]; // +1 for extra NULL to always terminate
			memset(&buffer, '\0', sizeof(buffer));
			int recv_len = recv(new_sock_fd, &buffer, BUF_SZ, 0);
			if (recv_len < 1) {
				break;
			}
			printf("RECV: message size is %db\n", recv_len);
			printf("MESG: %s\n", buffer);
		}

		close(new_sock_fd);
		printf("SERV: connection closed\n");
	}

	exit(EXIT_SUCCESS);
}
