// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "tmr.h"

void waste_time() {
	double x = 0.0;
	for (int i = 0; i < 1e6; i++) {
		x = (x + 0.01) * 1.01;
	}
}

int main(void) {
	tmr_s* tmr_p = tmr_init();

	waste_time();

	tmr_time(tmr_p, "since created");

	waste_time();

	tmr_time(tmr_p, "since 'since created'");

	for (int j = 0; j < 10; j++) {
		waste_time();
	}

	tmr_time(tmr_p, "after for loop");

	exit(EXIT_SUCCESS);
}
