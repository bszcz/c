// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#pragma once

#define NUM_TIMERS 64

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct tmr {
	clock_t last;
	clock_t log[NUM_TIMERS];
};
typedef struct tmr tmr_s;

tmr_s* tmr_init();

int tmr_time(tmr_s* tmr_p, char* msg);

int tmr_log(tmr_s* tmr_p, int timer_num);
