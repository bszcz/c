// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "tmr.h"

tmr_s* tmr_init() {
	tmr_s* tmr_p = malloc(sizeof(tmr_s));
	if (tmr_p == NULL) {
		return NULL;
	}
	tmr_p->last = clock();
	return tmr_p;
}

int tmr_time(tmr_s* tmr_p, char* msg) {
	clock_t now = clock();
	if (tmr_p == NULL) {
		return -1;
	}
	if (msg != NULL) {
		const double seconds = (double)(now - tmr_p->last) / CLOCKS_PER_SEC;
		fprintf(stderr, "tmr: %.6lf sec (%s)\n", seconds, msg);
	}
	tmr_p->last = now;
	return 0;
}


int tmr_log(tmr_s* tmr_p, int timer_num) {
	if (tmr_p == NULL) {
		return -1;
	}
	if (0 <= timer_num && timer_num < NUM_TIMERS) {
		tmr_p->last = clock();
		tmr_p->log[timer_num] = tmr_p->last;
	} else {
		return -2;
	}
	return timer_num;
}
