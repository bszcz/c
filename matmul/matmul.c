// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct sqmat {
	int size;
	double** elems;
};

struct sqmat sqmat_calloc(const int size) {
	struct sqmat M = {.size = size};
	M.elems = calloc(size, sizeof(double*));
	if (!M.elems) {
		printf("sqmat_random: cannot allocate memory\n");
		exit(EXIT_FAILURE);
	}
	M.elems[0] = calloc(size * size, sizeof(double));
	if (!M.elems[0]) {
		printf("sqmat_random: cannot allocate memory\n");
		free(M.elems);
		exit(EXIT_FAILURE);
	}
	for (int i = 1; i < size; i++) {
		M.elems[i] = M.elems[i - 1] + size;
	}
	return M;
}

void sqmat_free(const struct sqmat M) {
	free(M.elems[0]);
	free(M.elems);
}

int sqmat_equal(const struct sqmat A, const struct sqmat B) {
	if (A.size != B.size) {
		return 0;
	}
	for (int i = 0; i < A.size; i++) {
		for (int j = 0; j < A.size; j++) {
			double diff = A.elems[i][j] - B.elems[i][j];
			if (fabs(diff) > 1e-12) {
				return 0;
			}
		}
	}
	return 1;
}

struct sqmat sqmat_random(const int size) {
	struct sqmat M = sqmat_calloc(size);
	srand(time(NULL));
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			M.elems[i][j] = (double)rand() / (double)RAND_MAX;
		}
	}
	return M;
}

struct sqmat sqmat_mul(const struct sqmat A, const struct sqmat B) {
	if (A.size != B.size) {
		printf("sqmat_mul: size mismatch\n");
		exit(EXIT_FAILURE);
	}
	struct sqmat C = sqmat_calloc(A.size);
	for (int i = 0; i < A.size; i++) {
		for (int j = 0; j < A.size; j++) {
			for (int k = 0; k < A.size; k++) {
				C.elems[i][j] += A.elems[i][k] * B.elems[k][j];
			}
		}
	}
	return C;
}

struct sqmat sqmat_mul_swap_order(const struct sqmat A, const struct sqmat B) {
	if (A.size != B.size) {
		printf("sqmat_mul: size mismatch\n");
		exit(EXIT_FAILURE);
	}
	struct sqmat C = sqmat_calloc(A.size);
	for (int i = 0; i < A.size; i++) {
		for (int j = 0; j < A.size; j++) {
			for (int k = 0; k < A.size; k++) {
				C.elems[i][k] += A.elems[j][k] * B.elems[i][j];
			}
		}
	}
	return C;
}

struct sqmat sqmat_mul_trans(const struct sqmat A, const struct sqmat B) {
	if (A.size != B.size) {
		printf("sqmat_mul_trans: size mismatch\n");
		exit(EXIT_FAILURE);
	}
	struct sqmat C = sqmat_calloc(A.size);
	for (int i = 0; i < A.size; i++) {
		for (int j = 0; j < A.size; j++) {
			for (int k = 0; k < A.size; k++) {
				C.elems[i][j] += A.elems[i][k] * B.elems[j][k];
			}
		}
	}
	return C;
}

struct sqmat sqmat_trans_clone(const struct sqmat M) {
	struct sqmat MT = sqmat_calloc(M.size);
	for (int i = 0; i < M.size; i++) {
		for (int j = 0; j < M.size; j++) {
			MT.elems[i][j] = M.elems[j][i];
		}
	}
	return MT;
}

struct sqmat sqmat_mul_trans_clone(const struct sqmat A, const struct sqmat B) {
	struct sqmat BT = sqmat_trans_clone(B);
	struct sqmat C = sqmat_mul_trans(A, BT);
	sqmat_free(BT);
	return C;
}

void sqmat_trans_mutate(const struct sqmat M) {
	for (int i = 0; i < M.size; i++) {
		for (int j = i + 1; j < M.size; j++) {
			double tmp = M.elems[i][j];
			M.elems[i][j] = M.elems[j][i];
			M.elems[j][i] = tmp;
		}
	}
}

struct sqmat sqmat_mul_trans_mutate(const struct sqmat A, const struct sqmat B) {
	sqmat_trans_mutate(B);
	struct sqmat C = sqmat_mul_trans(A, B);
	sqmat_trans_mutate(B);
	return C;
}

int main(void) {
	const int size_step = 64;
	const int max_size = 2048;

	printf("#size mul mul_trans_clone mul_trans_mutate\n");
	for (int size = size_step; size <= max_size; size += size_step) {
		struct sqmat A = sqmat_random(size);
		struct sqmat B = sqmat_random(size);

		double t1 = omp_get_wtime();
		struct sqmat C1 = sqmat_mul(A, B);
		t1 = omp_get_wtime() - t1;

		double t2 = omp_get_wtime();
		struct sqmat C2 = sqmat_mul_trans_clone(A, B);
		t2 = omp_get_wtime() - t2;

		double t3 = omp_get_wtime();
		struct sqmat C3 = sqmat_mul_trans_mutate(A, B);
		t3 = omp_get_wtime() - t3;

		double t4 = omp_get_wtime();
		struct sqmat C4 = sqmat_mul_swap_order(A, B);
		t4 = omp_get_wtime() - t4;

		printf("%d %lf %lf %lf %lf\n", size, t1, t2, t3, t4);

		if (!sqmat_equal(C1, C2) || !sqmat_equal(C1, C3) || !sqmat_equal(C1, C4)) {
			printf("main: multiplication error\n");
			exit(EXIT_FAILURE);
		}

		sqmat_free(A);
		sqmat_free(B);
		sqmat_free(C1);
		sqmat_free(C2);
		sqmat_free(C3);
		sqmat_free(C4);
	}

	exit(EXIT_SUCCESS);
}
