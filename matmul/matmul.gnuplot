set key top left
set grid

p \
"matmul.txt" u 1:2:3 with yerrorbars pointtype 0 linecolor 1 title "mul", \
"matmul.txt" u 1:4:5 with yerrorbars pointtype 0 linecolor 2 title "mul_trans_clone", \
"matmul.txt" u 1:6:7 with yerrorbars pointtype 0 linecolor 3 title "mul_trans_mutate", \
\
"matmul.txt" u 1:2:3 with line linecolor 1 notitle, \
"matmul.txt" u 1:4:5 with line linecolor 2 notitle, \
"matmul.txt" u 1:6:7 with line linecolor 3 notitle

pause -1
