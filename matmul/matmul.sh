paste data/*.txt | awk '{
	if (NR == 1) {
		print $1, $2, $3, $4;
	} else {
		n = NF / 4;
		sum1 = 0;
		sum2 = 0;
		sum3 = 0;
		for (i = 1; i < NF; i += 4) {
			sum1 += $(i + 1);
			sum2 += $(i + 2);
			sum3 += $(i + 3);
		}
		mean1 = sum1 / n;
		mean2 = sum2 / n;
		mean3 = sum3 / n;
		sum1 = 0;
		sum2 = 0;
		sum3 = 0;
		for (i = 1; i < NF; i += 4) {
			sum1 += ($(i + 1) - mean1)^2;
			sum2 += ($(i + 2) - mean2)^2;
			sum3 += ($(i + 3) - mean3)^2;
		}
		stderr1 = sqrt(sum1 / n) / sqrt(n);
		stderr2 = sqrt(sum2 / n) / sqrt(n);
		stderr3 = sqrt(sum3 / n) / sqrt(n);
		print $1, mean1, stderr1, mean2, stderr1, mean3, stderr1;
	}
}'
