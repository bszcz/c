// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>

typedef union {
	double v[3];
	struct {
		double x;
		double y;
		double z;
	};
} vec3_t;
#define VEC3(x, y, z) {{x, y, z}}

int main(void) {
	vec3_t vec = VEC3(1.0, 2.0, 3.0);
	printf("vec.x    = %f\n", vec.x);
	printf("vec.v[0] = %f\n", vec.v[0]);
	exit(EXIT_SUCCESS);
}
