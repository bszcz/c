// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// old style C function argument declarations

#include <stdio.h>
#include <stdlib.h>

add_two(i1, i2) { // types not given, int assumed
	return i1 + i2;
}

float add_six(f1, f2, f3, i1, i2, i3)
float f1, f2, f3;
int i1, i2, i3;
{
	return f1 + f2 + f3 + i1 + i2 + i3;
}

void mem_a(a, N)
int** a;
const int N;
{
	*a = (int*)malloc(N * sizeof(int));
}

void set_a(a, N)
int* a;
const int N;
{
	const int mul = 2;
	for (int i = 0; i < N; i++) {
		a[i] = mul * i;
	}
}

void put_a(a, N)
int* a;
const int N;
{
	for (int i = 0; i < N; i++) {
		printf("a[%d] = %d \n", i, a[i]);
	}
}

int main(void) {
	printf("sum2 = %d \n", add_two(31, 7));
	printf("sum6 = %f \n", add_six(0.1, 0.2, 0.3, 4, 5, 6));

	const int N = 10;
	int* a;
	mem_a(&a, N);
	set_a(a, N);
	put_a(a, N);

	exit(EXIT_SUCCESS);
}
