// Copyright (c) 2012-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void for_loop(int f, int i_max, int j_max) {
	switch (f) {
	case 0:
		for (int i = 0; i < i_max; i++) {
			for (int j = 0; j < j_max; j++) {
				;
			}
		}
		break;
	case 1:
		for (int i = 1; i <= i_max; i++) {
			for (int j = 1; j <= j_max; j++) {
				;
			}
		}
		break;
	case 2:
		for (int i = i_max; i != 0; i--) {
			for (int j = j_max; j != 0; j--) {
				;
			}
		}
		break;
	case 3:
		for (int i = i_max; i--;) {
			for (int j = j_max; j--;) {
				;
			}
		}
		break;
	}
}

int main(void) {
	const int f_max = 4;
	const int pow_min = 4;
	const int pow_max = 8;
	const int j_max = 1 << 24;

	clock_t time_min;
	for (int pow = pow_min; pow < pow_max; pow++) {
		printf("%d ", pow);
		for (int f = 0; f < f_max; f++) {
			clock_t start = clock();
			for_loop(f, 1 << pow, j_max);
			clock_t stop = clock();
			if (f == 0) {
				time_min = stop - start;
			}
			printf("%f ", (double)(stop - start) / time_min);
		}
		printf("\n");
	}

	exit(EXIT_SUCCESS);
}
