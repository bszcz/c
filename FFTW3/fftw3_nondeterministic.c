// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <fftw3.h>
#include <inttypes.h>
#include <openssl/sha.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	const int arr_seed = 666;
	const int arr_side = 128;
	const int arr_size = arr_side * arr_side * sizeof(fftw_complex);
	fftw_complex* arr_fourier  = fftw_malloc(arr_size);
	fftw_complex* arr_physical = fftw_malloc(arr_size);
	srand(arr_seed);
	for (int i = 0; i < arr_side*arr_side; i++) {
		for (int k = 0; k < 2; k++) { // real/imag part
			arr_fourier [i][k] = (double)rand() / RAND_MAX;
			arr_physical[i][k] = 0.0;
		}
	}

	const bool deterministic = false;
	fftw_plan plan;
	if (deterministic) {
		plan = fftw_plan_dft_2d(arr_side, arr_side, arr_fourier, arr_physical, FFTW_BACKWARD, FFTW_ESTIMATE);
	} else {
		plan = fftw_plan_dft_2d(arr_side, arr_side, arr_fourier, arr_physical, FFTW_BACKWARD, FFTW_MEASURE);
	}
	fftw_execute(plan);

	unsigned char md[SHA_DIGEST_LENGTH];
	SHA1((const unsigned char*)arr_physical, (unsigned long)arr_size, md);
	for (int i = 0; i < SHA_DIGEST_LENGTH; i++) {
		printf("%02x", md[i]);
	}
	printf("\n");

	exit(EXIT_SUCCESS);
}
