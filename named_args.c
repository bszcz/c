// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct range_args {
	int start;
	int step;
	int stop;
};
typedef struct range_args range_args_s;

int set_step(const range_args_s args) {
	if (args.step == 0) {
		return 1;
	} else {
		return args.step;
	}
}

void print_range(const range_args_s const_args) {
	if (const_args.start > const_args.stop) {
		printf("[error: start > stop]");
		return;
	}

	range_args_s args = const_args;
	args.step = set_step(args);
	if (args.step < 0) {
		printf("[error: step < 0]");
		return;
	}

	printf("[");
	for (int i = args.start; i < args.stop; i += args.step) {
		printf("%d", i);
		if (i + args.step < args.stop) {
			printf(", ");
		}
	}
	printf("]");
}

int main(void) {
	print_range((range_args_s){});
	printf("\n");
	print_range((range_args_s){.step = -1, .stop = 10});
	printf("\n");
	print_range((range_args_s){.start = +5, .stop = -5});
	printf("\n");
	print_range((range_args_s){.stop = 10});
	printf("\n");
	print_range((range_args_s){.start = 1, .stop = 10});
	printf("\n");
	print_range((range_args_s){.start = -5, .stop = +5});
	printf("\n");
	print_range((range_args_s){.start = 4, .stop = 10, .step = 2});

	exit(EXIT_SUCCESS);
}
