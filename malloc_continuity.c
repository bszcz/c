// Copyright (c) 2012-2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>

void null_check(const void* p) {
	printf("have to check for NULL\n");
	if (p == NULL) {
		exit(EXIT_FAILURE);
	}
}

const int** alloc2(const int N) {
	printf("* alloc2():\n");
	const int** a = malloc(N * sizeof(int*));
	null_check(a);
	for (int i = 0; i < N; i++) {
		a[i] = malloc(N * sizeof(int));
		null_check(a[i]);
	}
	return a;
}

const int** alloc1(const int N) {
	printf("* alloc1():\n");
	const int** a = malloc(N * sizeof(int*));
	null_check(a);
	a[0] = malloc(N * N * sizeof(int));
	null_check(a[0]);
	for (int i = 1; i < N; i++) {
		a[i] = a[0] + (N * i);
	}
	return a;
}

void show_gaps(const int** a, const int N) {
	printf("* show_gaps():\n");
	int* p = (int*)&(a[0][0]);

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			int gap = (&a[i][j] - p) - 1;
			if (i == 0 && j == 0) {
				gap = 0; // first element correction
			}
			printf("%ld ", gap);
			p = (int*)&(a[i][j]);
		}
		printf("\n");
	}
}

int main(void) {
	const int N = 16;

	const int** a2 = alloc2(N);
	show_gaps(a2, N);

	const int** a1 = alloc1(N);
	show_gaps(a1, N);

	exit(EXIT_SUCCESS);
}
