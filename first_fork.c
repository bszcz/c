// Copyright (c) 2012 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <unistd.h>
#include <stdio.h>

int main(void) {
	fork();

	puts("This message should appear twice.");

	return 0;
}
