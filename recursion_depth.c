// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>

void recurse() {
	static int depth = 0;
	printf("depth = %d\n", depth);
	depth++;
	recurse();
}

int main(void) {
	recurse();
	exit(EXIT_SUCCESS);
}
