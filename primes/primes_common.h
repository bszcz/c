// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct node {
	int num;
	struct node* next;
};

struct node* addprime(const int num);

void output(struct node* primes, const int maxnum, const int t, const int argc);

void destroy(struct node* primes);
