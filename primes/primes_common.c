// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "primes_common.h"

struct node* addprime(const int num) {
	struct node* n = malloc(sizeof(struct node));
	n->num = num;
	n->next = NULL;
	return n;
}

void output(struct node* primes, const int maxnum, const int t, const int argc) {
	if (argc > 2) {
		while (primes) {
			printf("%d\n", primes->num);
			primes = primes->next;
		}
	} else {
		printf("%d %.6g\n", maxnum, (double)t / CLOCKS_PER_SEC);
	}
}

void destroy(struct node* primes) {
	struct node* p = primes;
	while (p) {
		struct node* n = p->next;
		free(p);
		p = n;
	}
}
