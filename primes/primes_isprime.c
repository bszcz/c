// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "primes_common.h"
#include <math.h>

int isprime(int num) {
	int maxdiv = (int)sqrt(num);
	for (int div = 3; div <= maxdiv; div += 2) {
		if (num % div == 0) {
			return 0;
		}
	}
	return 1;
}

int main(int argc, char** argv) {
	int maxnum = strtol(argv[1], NULL, 10);

	time_t t = clock();

	struct node* primes = addprime(2);
	struct node* current = primes;
	current->next = addprime(3);
	current = current->next;
	current->next = addprime(5);
	current = current->next;
	current->next = addprime(7);
	current = current->next;

	for (int num = 11; num <= maxnum; num += 2) {
		if (isprime(num)) {
			current->next = addprime(num);
			current = current->next;
		}
	}

	t = clock() - t;

	output(primes, maxnum, t, argc);
	destroy(primes);
	exit(EXIT_SUCCESS);
}
