// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "primes_common.h"
#include <string.h>

inline int index(const int num) {
	return (num / 2) - 1;
}

int main(int argc, char** argv) {
	const int maxnum = strtol(argv[1], NULL, 10);

	time_t t = clock();

	const int len = (maxnum - 1) / 2;
	char* isprime = malloc(len * sizeof(char));
	if (isprime == NULL) {
		printf("isprime == NULL\n");
		exit(EXIT_FAILURE);
	}
	memset(isprime, 1, len);

	int num = 3;
	while (num <= maxnum) {
		int rem = 3 * num;
		while (rem <= maxnum) {
			isprime[index(rem)] = 0;
			rem += 2 * num;
		}
		num += 2;
		while (num <= maxnum && ! isprime[index(num)]) {
			num += 2;
		}
	}

	struct node* primes = addprime(2);
	struct node* current = primes;
	num = 3;
	while (num <= maxnum) {
		if (isprime[index(num)]) {
			current->next = addprime(num);
			current = current->next;
		}
		num += 2;
	}

	t = clock() - t;

	output(primes, maxnum, t, argc);
	destroy(primes);
	free(isprime);
	exit(EXIT_SUCCESS);
}
