// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
first column of "const" is unnecessary if "const struct" is used but ensures
that the members are still "const" even if a struct is not defined as "const"
*/
struct parameters_t {
	const double frac;
	const int num;
	const int* const arr; // 2nd "const" for elements of "arr", note position of "*"
};

struct parameters_t* ParametersAlloc(const int num, const double frac, const int arr0, const int arr1) {
	int* arrTemp = calloc(2, sizeof(int)); // obviously, never use magic numbers
	arrTemp[0] = arr0;
	arrTemp[1] = arr1;

	struct parameters_t paraTemp = {
		.frac = frac,
		.num  = num,
		.arr  = arrTemp
	};

	size_t structSize = sizeof(struct parameters_t);
	struct parameters_t* paraPtr = calloc(1, structSize);
	memcpy(paraPtr, &paraTemp, structSize);
	return paraPtr;
}

int main(void) {
	// these could come from "argc/argv":
	double frac = 0.666;
	int num = 137;
	int arr0 = 370;
	int arr1 = 371;

	// "const struct" not used but members are "const" anyway
	struct parameters_t* paraPtr = ParametersAlloc(num, frac, arr0, arr1);

	/*
	paraPtr->frac   = 0.0;  // error: assignment of read-only member ‘frac’
	paraPtr->num    = 0;    // error: assignment of read-only member ‘num’
	paraPtr->arr    = NULL; // error: assignment of read-only member ‘arr’
	paraPtr->arr[0] = 0;    // error: assignment of read-only location ‘*paraPtr->arr’
	*/

	printf("paraPtr->frac   = %f\n", paraPtr->frac);
	printf("paraPtr->num    = %d\n", paraPtr->num);
	printf("paraPtr->arr[0] = %d\n", paraPtr->arr[0]);
	printf("paraPtr->arr[1] = %d\n", paraPtr->arr[1]);

	return 0;
}
