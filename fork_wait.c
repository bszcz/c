// Copyright (c) 2012, 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

int main(void) {
	int is_parent = fork();

	if (is_parent) {
		puts("\nI'm the parent and I'll wait for my child.");
		wait(0);
		puts("\nI'm the parent and I'm finished waiting.");
	} else {
		sleep(1);
		puts("\nI'm the  child and I'll derp for my herp.");
		sleep(1);
		puts("\nI'm the  child and I'm finished derping.");
	}

	return 0;
}
