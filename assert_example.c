// Copyright (c) 2011 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// Defining `NDEBUG` disables `assert()`.
// Must define before including `assert.h`.

//#define NDEBUG

#include <assert.h>
#include <stdio.h>

int main(void) {
	int b = 2;

	assert(b > 1);
	printf("b > 1\n");

	assert(b > 3);
	printf("b > 3\n");

	return 0;
}
