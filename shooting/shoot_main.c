// Copyright (c) 2013-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <getopt.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double xi_step = 1.0 / (1 << 10);

struct vec {
	double a;
	double b;
	double axi;
	double bxi;
};
typedef struct vec vec_s;


void vec_printf(const vec_s pt) {
	printf("%f %f %f %f\n", pt.a, pt.b, pt.axi, pt.bxi);
}

double dist(const vec_s pt) {
	return sqrt(
		       pow(pt.a   - 0.0, 2) +
		       pow(pt.b   - 1.0, 2) +
		       pow(pt.axi - 0.0, 2) +
		       pow(pt.bxi - 0.0, 2)
	       );
}

void vec_fprintf(FILE* stream, const vec_s pt) {
	fprintf(stream, "%f %f %f %f\n", pt.a, pt.b, pt.axi, pt.bxi);
}

double df_a_xi(const vec_s pt, const double delta_d, const double vel) {
	return pt.axi;
}

double df_b_xi(const vec_s pt, const double delta_d, const double vel) {
	return pt.bxi;
}

double df_axi_xi_num(const vec_s pt, const double delta_d, const double vel) {
	return -1.0*(
		       ((pt.axi + pt.bxi)*(delta_d - 1)*vel - delta_d
		        + (2*delta_d - 1 - (delta_d - 1)*pt.b)*pt.b)*pt.a
		       - ((delta_d - 1)*pt.a*pt.a*pt.a + pt.axi*delta_d*vel
		          + (2*(delta_d - 1)*pt.b - delta_d)*pt.a*pt.a)
	       );
}

double df_axi_xi_den(const vec_s pt, const double delta_d, const double vel) {
	return ((pt.a + pt.b)*(delta_d - 1) - delta_d)*delta_d;
}

double df_bxi_xi_num(const vec_s pt, const double delta_d, const double vel) {
	return -1.0*(
		       (2*delta_d - 1)*pt.b*pt.b  - pt.bxi*delta_d*vel
		       + (pt.axi*vel - pt.b*pt.b)*(delta_d - 1)*pt.b
		       + ((delta_d - 1)*pt.bxi*vel + delta_d)*pt.b
		       - ((pt.a + 2*pt.b)*(delta_d - 1)*pt.b - (pt.b + 1)*delta_d)*pt.a
	       );
}

double df_bxi_xi_den(const vec_s pt, const double delta_d, const double vel) {
	return ((pt.a + pt.b)*(delta_d - 1) - delta_d)*delta_d;
}

vec_s df(const vec_s pt, const double delta_d, const double vel) {
	vec_s df;
	df.a = df_a_xi(pt, delta_d, vel);
	df.b = df_b_xi(pt, delta_d, vel);
	df.axi = df_axi_xi_num(pt, delta_d, vel) / df_axi_xi_den(pt, delta_d, vel);
	df.bxi = df_bxi_xi_num(pt, delta_d, vel) / df_bxi_xi_den(pt, delta_d, vel);
	return df;
}

vec_s euler(const vec_s pt_old, const double delta_d, const double vel) {
	vec_s f = df(pt_old, delta_d, vel);
	vec_s pt_new = pt_old;
	pt_new.a   += xi_step * f.a;
	pt_new.b   += xi_step * f.b;
	pt_new.axi += xi_step * f.axi;
	pt_new.bxi += xi_step * f.bxi;
	return pt_new;
}

vec_s midpoint(const vec_s pt_old, const double delta_d, const double vel) {
	vec_s f = df(pt_old, delta_d, vel);
	vec_s pt_mid = pt_old;
	pt_mid.a   += xi_step / 2.0 * f.a;
	pt_mid.b   += xi_step / 2.0 * f.b;
	pt_mid.axi += xi_step / 2.0 * f.axi;
	pt_mid.bxi += xi_step / 2.0 * f.bxi;
	f = df(pt_mid, delta_d, vel);
	vec_s pt_new = pt_old;
	pt_new.a   += xi_step * f.a;
	pt_new.b   += xi_step * f.b;
	pt_new.axi += xi_step * f.axi;
	pt_new.bxi += xi_step * f.bxi;
	return pt_new;
}

int main(int argc, char** argv) {
	double delta_d = 1.0;
	double vel = 0.5;
	int num_steps = 100000;

	int opt;
	while ((opt = getopt(argc, argv, "d:n:v:")) != -1) {
		switch (opt) {
		case 'd':
			delta_d = strtod(optarg, NULL);
			break;
		case 'n':
			num_steps = strtol(optarg, NULL, 10);
			break;
		case 'v':
			vel = strtod(optarg, NULL);
			break;
		default:
			printf("usage: %s -d <delta_d> -n <num_steps -v <velocity>\n",
			       argv[0]);
		}
	}


	FILE* data1 = fopen("shoot_data_order_1.txt", "wb");
	FILE* data2 = fopen("shoot_data_order_2.txt", "wb");
	fprintf(data1, "# a b axi bxi (euler) \n");
	fprintf(data2, "# a b axi bxi (midpoint) \n");
	vec_s pt1 = {1.0 - 0.0001, 0.0001, 0.0, 0.0};
	vec_s pt2 = pt1;
	vec_fprintf(data1, pt1);
	vec_fprintf(data2, pt2);
	double min = 100.0;
	for (int step = 0; step < num_steps; step++) {
		pt1 = euler(pt1, delta_d, vel);
		pt2 = midpoint(pt2, delta_d, vel);
		vec_fprintf(data1, pt1);
		vec_fprintf(data2, pt2);
		if (dist(pt2) < min) {
			min = dist(pt2);
		}
	}
	printf("# delta_d velocity minimum_distance\n");
	printf("%.16lf %.16lf %.16lf\n", delta_d, vel, min);
	fclose(data1);
	fclose(data2);

	exit(EXIT_SUCCESS);
}
