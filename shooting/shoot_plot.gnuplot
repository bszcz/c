set grid
set xlabel "b / bxi"
set ylabel "a / axi"
set xrange [ -0.1 : 1.1 ]
set yrange [ -0.1 : 1.1 ]
set term png size 1200, 900
set output "shoot_plot.png"
p \
"shoot_data_order_1.txt" u 2:1 w l, "shoot_data_order_1.txt" u 4:3 w l, \
"shoot_data_order_2.txt" u 2:1 w l, "shoot_data_order_2.txt" u 4:3 w l
